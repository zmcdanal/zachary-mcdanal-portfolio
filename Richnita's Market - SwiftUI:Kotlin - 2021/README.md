# README #

Richnita's Market

### The Challenge ###

* Your mission, should you choose to accept it, is to showcase your coding talents and engineer the following:

    •   The owner of Richnita’s Market has a requirement to build an in-store android app to help improve their customer’s experience.
    
    •   Richnita’s Market wants the ability for customers to scan items using their phones and add those items to a virtual shopping cart. Each item added should appear in the customer’s order. The item should be displayed with the item’s name and price. In addition to allowing users to add and remove items from their virtual shopping cart, your app should also display a calculated total of all items in order of scan.
    
    •   Richnita’s Market will provide a database of the inventory of items in the format shown below. Each item is identified using a QR code. Customers must be able to use their phone’s camera to scan the QR code to retrieve the item’s information.
    
    •   (Bonus points if you can feature a “sort” function for item names and price)
    
    •   (Bonus points if you can use the QR code to display the nutritional facts)
    
    ### QR Links ###
    
    * https://zxing.org/w/chart?cht=qr&chs=350x350&chld=L&choe=UTF-8&chl=0001
    * https://zxing.org/w/chart?cht=qr&chs=350x350&chld=L&choe=UTF-8&chl=0002
    * https://zxing.org/w/chart?cht=qr&chs=350x350&chld=L&choe=UTF-8&chl=0003

### How I did it? ###

* Followed your basic outline
* Coded in Kotlin
* Used Library 'com.journeyapps:zxing-android-embedded:3.6.0' for QR reader
* Data Class used for easy, future Database implementation
* Json file used to simulate database usage
* Custom Adapter for ListView
* Tested on real Google Pixel 3a

### Mystery ###

* There is a hidden easter egg regarding this app. Can you find it?
