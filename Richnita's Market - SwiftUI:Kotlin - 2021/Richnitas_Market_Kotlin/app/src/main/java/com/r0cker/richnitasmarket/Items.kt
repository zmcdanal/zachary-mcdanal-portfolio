package com.r0cker.richnitasmarket

data class Items (val image: Int, val item: String, val price: Double, var quantity: Int) {

    fun addItems() : Double {
        return price * quantity
    }
}