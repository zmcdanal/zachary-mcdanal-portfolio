package com.r0cker.richnitasmarket

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import com.google.zxing.integration.android.IntentIntegrator
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.StandardCharsets

class MainActivity : AppCompatActivity(), CompoundButton.OnCheckedChangeListener {

    // Variables and declarations
    private val REQUEST_CODE = 1
    private lateinit var scanBtn: Button
    private lateinit var listView: ListView
    private lateinit var totalTV: TextView
    private lateinit var sortSwitch: SwitchCompat
    private var arrayList: ArrayList<Items> = ArrayList()
    private var adapter: MyAdapter? = null
    private var currentSortOption = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // FindViewById's
        scanBtn = findViewById(R.id.scanItemBtn)
        listView = findViewById(R.id.itemList)
        totalTV = findViewById(R.id.totalTV)
        sortSwitch = findViewById(R.id.sortSwitch)

        // Scan qr code button set up here
        scanBtn.setOnClickListener {
            val integrator = IntentIntegrator(this)
            integrator.setRequestCode(REQUEST_CODE)
            integrator.setOrientationLocked(false)
            integrator.initiateScan()
        }
        // Sort switch on click set here
        sortSwitch.setOnCheckedChangeListener(this)
        // Populate page with alert dialog box to delete item, then refresh page
        listView.setOnItemClickListener { parent, view, position, id ->
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Delete Item")
            if (arrayList[position].quantity > 1) { // There is more than 1 item so either delete all or take one quantity away
                builder.setMessage("Do you wish to delete one or all items?")
                builder.setPositiveButton("Delete All") { dialog, which ->
                    arrayList.remove(arrayList[position])
                    sortArray()
                    totalPrice()
                }
                builder.setNeutralButton("Delete One") { dialog, which ->
                    arrayList[position].quantity -= 1
                    sortArray()
                    totalPrice()
                }
            } else { // There is less than one item so delete data as whole
                builder.setMessage("Do you wish to delete this item?")
                builder.setNeutralButton("Delete") { dialog, which ->
                    arrayList.remove(arrayList[position])
                    sortArray()
                    totalPrice()
                }
            }

            builder.setNegativeButton("Cancel") { dialog, which ->
            }
            builder.show()
        }
    }

    //Class MyAdapter
    class MyAdapter(private val context: Context, private val arrayList: java.util.ArrayList<Items>) : BaseAdapter() {
        private lateinit var image: ImageView
        private lateinit var item: TextView
        private lateinit var quantity: TextView
        private lateinit var price: TextView
        override fun getCount(): Int {
            return arrayList.size
        }
        override fun getItem(position: Int): Any {
            return position
        }
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
            val cView = LayoutInflater.from(context).inflate(R.layout.list_layout, parent, false)
            if (arrayList.isNotEmpty()) {
                // findViewById's
                image = cView.findViewById(R.id.itemImage)
                item = cView.findViewById(R.id.itemName)
                quantity = cView.findViewById(R.id.quantity)
                price = cView.findViewById(R.id.price)

                // Image
                image.setImageResource(arrayList[position].image)
                // Item Name
                val itemName = " " + arrayList[position].item
                item.text = itemName
                // Quantity
                quantity.text = arrayList[position].quantity.toString()
                // Price
                val rounded = String.format("%.2f", arrayList[position].addItems())
                val priceString = "$$rounded"
                price.text = priceString
            }
            return cView
        }
    }

    // Process the result from the zxing QR code scan
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode != REQUEST_CODE) {
            super.onActivityResult(requestCode, resultCode, data)
            return
        }

        val result = IntentIntegrator.parseActivityResult(resultCode, data)
        if(result != null) {

            if(result.contents == null) {
                Toast.makeText(this, "Scan cancelled", Toast.LENGTH_LONG).show()
            }
            else
            {
                val json: String
                val `is` = applicationContext.assets.open("mockItemDatabase.json")
                val size = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, StandardCharsets.UTF_8)
                try {
                    val outerMostObject = JSONObject(json)
                        val items = outerMostObject.getJSONObject("items")
                        val code = items.getJSONObject(result.contents)
                        val image = code.getInt("image")
                        val itemName = code.getString("item")
                        val price = code.getDouble("price")

                        var doesContain = false
                        for (item in arrayList) {
                            if (item.item == itemName) {
                                item.quantity += 1
                                doesContain = true
                            }
                        }
                        if (!doesContain) {
                            val drawable: Int = image
                            arrayList.add(Items(drawable, itemName, price, 1))
                        }
                    //}
                } catch (e: JSONException) {
                    println("Error parsing json")
                }
            }
            // Update page
            totalPrice()
            sortArray()
            listView.invalidateViews()
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

    // Switch onChecked method
    override fun onCheckedChanged(p0: CompoundButton?, sortOption: Boolean) {

        if (sortOption) {
            println("price")
            currentSortOption = 1
            sortArray()
        } else {
            println("a-z")
            currentSortOption = 0
            sortArray()
        }

    }

    // Sorts the array with either a-z or by price of items * quantity
    private fun sortArray() {
            if (currentSortOption == 0) {
                val sortedArray = arrayList.sortedBy { it.item }
                arrayList = ArrayList(sortedArray)
                adapter = MyAdapter(this, arrayList)
                listView.adapter = adapter
            } else {
                val sortedArray = arrayList.sortedByDescending { it.addItems() }
                arrayList = ArrayList(sortedArray)
                listView.invalidateViews()
                adapter = MyAdapter(this, arrayList)
                listView.adapter = adapter

            }

    }

    // Calculates the total price of all items and populates the page
    private fun totalPrice() {
        var total = 0.0
        for (quantity in arrayList) {
            total += quantity.addItems()
        }
        val rounded = String.format("%.2f", total)
        val finishedTotal = "$$rounded"
        totalTV.text = finishedTotal
    }
}