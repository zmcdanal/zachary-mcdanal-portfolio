//
//  Richnitas_Market_iOSApp.swift
//  Richnitas Market iOS
//
//  Created by Zakarias McDanal on 6/21/21.
//

import SwiftUI

@main
struct Richnitas_Market_iOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
