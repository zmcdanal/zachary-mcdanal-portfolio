//
//  ContentView.swift
//  Richnitas Market iOS
//
//  Created by Zakarias McDanal on 6/21/21.
//

import SwiftUI
import CodeScanner

// Custom Object Struct
struct Items: Identifiable {
    
    var id = UUID()
    var image: String
    var item: String
    var price: Double
    var quantity: Int
    
    init(image: String, item: String, price: Double, quantity: Int) {
        self.image = image
        self.item = item
        self.price = price
        self.quantity = quantity
    }
}

// Custom List Row
struct RowView : View {
    
    var item1 : Items
    
    var body: some View {
        HStack() {
            Image(item1.image).resizable().frame(width: 40.0, height: 40.0).padding()
            Text(item1.item).padding()
            Text(String(item1.quantity)).padding()
            Text("$\(item1.price, specifier: "%.2f")").padding()
        }
    }
}

struct ContentView: View {
    
    // Variables
    @State private var isShowingScanner = false
    @State var toggleSort = false
    @State var listarray = [Items]()
    @State var total = 0.00
    
    
    var body: some View {
        
        // Title
        VStack() {
            Text("Richnita's Market")
                .bold()
                .font(.title)
            Divider()
            
            // Scanner Title and Button
            HStack() {
                Text("Click here to scan item ->").padding()
                Button(action: {self.isShowingScanner = true} , label: {
                    Text("Scan").padding(.vertical, 6.0)
                        .padding([.leading, .trailing], 18.0)
                        .background(Color.blue)
                        .foregroundColor(Color.white)
                        .cornerRadius(10.0)
                    
                }).sheet(isPresented: $isShowingScanner) {
                    CodeScannerView(codeTypes: [.qr], simulatedData: "Some simulated data", completion: self.handleScan)
                }
            }
            
            // Sort Title and Button
            HStack() {
                Text("Sort:").padding()
                Text("A-Z")
                Toggle(isOn: $toggleSort){}.labelsHidden().onChange(of: toggleSort, perform: { value in
                    if (toggleSort) {
                        listarray.sort {$0.price > $1.price}
                    } else {
                        listarray.sort {$0.item < $1.item}
                    }
                })
                Text("Price")
                Spacer()
            }
            
            // Category Title
            HStack() {
                Text("Item").padding()
                Text("Quantity").padding()
                Text("Price").padding()
            }
            Divider()
            
            // Item List
            List(listarray){ j in RowView(item1: j)
                
            }
            
            Divider()
            
            // Total title and Price
            HStack() {
                Spacer()
                Text("Total: ").padding()
                Text("$\(total, specifier: "%.2f")").padding()
            }
            Spacer()
        }
    }
    
    // Scanner Handler
    private func handleScan(result: Result<String, CodeScannerView.ScanError>) {
        self.isShowingScanner = false
        switch result {
        case .success(let data):
            print("Success with \(data)")
            jsonHandler(code: data)
            
        case .failure(let error):
            print("Scanning failed \(error)")
        }
    }
    
    // MockDatabase handler
    func jsonHandler(code: String) {
        //Get the path to our BeerData.json file
        if let path = Bundle.main.path(forResource: "mockItemDatabase", ofType: ".json") {
            
            //Create URL with path
            let url = URL(fileURLWithPath: path)
            
            do {
                
                let data = try Data.init(contentsOf: url)
                
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                
                guard let json = jsonObj
                else {print("Parse Failed to Unwrap the Optional"); return}
                
                //Loop through 1st level objects
                for firstLevelItem in json {
                    //Try to convert 1st level object into a [String: Any]
                    guard let object = firstLevelItem as? [String: Any],
                          //Get the value of our properties for the current object
                          let items = object["item"] as? [String: Any]
                    
                    
                    else { continue }
                    var itemCheck: [String: Any]
                    
                    if let item1 = items[code] as? [String: Any] {
                        itemCheck = item1
                    } else {
                        itemCheck = (items["other"] as? [String: Any])!
                    }
                          
                    let name = (itemCheck["item"] as? String)!
                    let image = (itemCheck["image"] as? String)!
                    let price = (itemCheck["price"] as? Double)!
                    
                    // Array Entries
                    var tempList = [Items]()
                    var doesContain = false
                    for i in listarray {
                        if i.item == name {
                            doesContain = true
                            let newQuan = i.quantity + 1
                            let newPrice = i.price + price
                            tempList.append(Items(image: i.image, item: i.item, price: newPrice, quantity: newQuan))
                        } else {
                            tempList.append(Items(image: i.image, item: i.item, price: i.price, quantity: i.quantity))
                        }
                    }
                    if (listarray.isEmpty || !doesContain) {
                        tempList.append(Items(image: image, item: name, price: price, quantity: 1))
                    }
                    listarray.removeAll()
                    listarray.append(contentsOf: tempList)
                    
                    total = 0.00
                    for i in listarray {
                        total += i.price
                    }
                    
                }
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
