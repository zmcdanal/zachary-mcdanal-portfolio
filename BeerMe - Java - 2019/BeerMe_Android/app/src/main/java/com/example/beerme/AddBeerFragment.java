package com.example.beerme;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddBeerFragment extends Fragment {

    // Various Variables
    private static final String TAG = "AddBeerFragment";
    private BeersTried beersTried;
    private Listener mListener;

    public AddBeerFragment() {
    }

    public static AddBeerFragment newInstance(BeersTried beersTried) {

        Bundle args = new Bundle();
        args.putSerializable(TAG, beersTried);
        AddBeerFragment fragment = new AddBeerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof AddBeerFragment.Listener) {
            mListener = (AddBeerFragment.Listener) context;
        }
    }

    // Interface
    public interface Listener {
        void submitNewBeer(BeersTried beersTried);
        void submitEditedBeer(BeersTried beersTried);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_beer, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null && getView() != null) {

            beersTried = (BeersTried) getArguments().getSerializable(TAG);

            // findViewByIds
            TextView pagetitleTV = getView().findViewById(R.id.pageTitleTV);
            ImageButton submitBtn = getView().findViewById(R.id.submitImgBtn);
            final EditText ratingET = getView().findViewById(R.id.ratingET);
            final EditText nameET = getView().findViewById(R.id.nameET);
            final EditText abvET = getView().findViewById(R.id.abvET);
            final EditText priceET = getView().findViewById(R.id.priceET);
            final EditText noteET = getView().findViewById(R.id.noteET);
            TextView submitBtnText = getView().findViewById(R.id.submitBtnTextTv);

            TextView amountBeerTV = getView().findViewById(R.id.amountDrankTV);
            final EditText amountBeerET = getView().findViewById(R.id.amountDrankET);

            if (beersTried != null) {
                amountBeerTV.setVisibility(TextView.VISIBLE);
                amountBeerET.setVisibility(TextView.VISIBLE);

                // set texts for page
                String editBeer = "Edit This Beer";
                String saveChanges = "Save Changes";
                pagetitleTV.setText(editBeer);
                amountBeerET.setText(String.valueOf(beersTried.getAmountDrank()));
                ratingET.setText(beersTried.getStringRating());
                nameET.setText(beersTried.getName());
                abvET.setText(beersTried.getStringABV());
                priceET.setText(beersTried.getStringCost());
                noteET.setText(beersTried.getNotes());
                submitBtnText.setText(saveChanges);
            } else {
                amountBeerTV.setVisibility(TextView.INVISIBLE);
                amountBeerET.setVisibility(TextView.INVISIBLE);

                String addABeer = "Add a Beer";
                String addBeer = "Add Beer";
                pagetitleTV.setText(addABeer);
                ratingET.setText("");
                nameET.setText("");
                abvET.setText("");
                priceET.setText("");
                noteET.setText("");
                submitBtnText.setText(addBeer);

            }

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkForEmptyEditTexts()) {
                            String name = nameET.getText().toString();
                            int rating = Integer.parseInt(ratingET.getText().toString());
                            double abv = Double.parseDouble(abvET.getText().toString());
                            double price = Double.parseDouble(priceET.getText().toString());
                            String note = noteET.getText().toString();

                            // Rating check
                            if (rating < 1 || rating > 5) {
                                Toast.makeText(getContext(), "Please rate only between 1 and 5",
                                        Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                // Get current date
                                Date c = Calendar.getInstance().getTime();
                                SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
                                String formattedDate = df.format(c);


                                if (beersTried != null) {
                                    String amountDrankInt = amountBeerET.getText().toString().trim();
                                    // Edited beer
                                    if (!amountDrankInt.equals("")) {
                                        beersTried = new BeersTried(beersTried.getId(), name, rating, price, abv, beersTried.getLastDrank(), Integer.parseInt(amountDrankInt), note, null);
                                        mListener.submitEditedBeer(beersTried);
                                    } else {
                                        Toast.makeText(getContext(), "Please leave no blanks", Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                } else {
                                    // New beer
                                    String id = name + rating;
                                    beersTried = new BeersTried(id, name, rating, price, abv, formattedDate, 1, note, null);
                                    mListener.submitNewBeer(beersTried);
                                }
                            }
                        } else {
                            Toast.makeText(getContext(), "Please leave no blanks", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });
            }

    }

    // Checks to make sure edit texts arent empty when submit is pressed
    private boolean checkForEmptyEditTexts() {
        if (getView() != null) {
            EditText ratingET = getView().findViewById(R.id.ratingET);
            EditText nameET = getView().findViewById(R.id.nameET);
            EditText abvET = getView().findViewById(R.id.abvET);
            EditText priceET = getView().findViewById(R.id.priceET);

            if (ratingET.getText().toString().trim().equals("") || nameET.getText().toString().trim().
                    equals("") || abvET.getText().toString().trim().equals("") || priceET.getText()
                    .toString().trim().equals("")) {
                return false;
            }
        }

        return true;
    }
}
