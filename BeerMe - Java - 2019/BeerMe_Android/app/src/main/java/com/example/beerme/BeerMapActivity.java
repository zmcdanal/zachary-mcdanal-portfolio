package com.example.beerme;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

public class BeerMapActivity extends AppCompatActivity implements OnMapReadyCallback , LocationListener, GoogleMap.OnMapLongClickListener,
        GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener, SearchView.OnQueryTextListener {

    //variables for use
    private static final String TAG = "BeerMapActivity";
    public static final int TO_SELECTED_BREWERY = 1;
    public static final int TO_PROFILE = 2;
    private static final int REQUEST_LOCATION_PERMISSIONS = 0x101;
    private GoogleMap mMap;
    private Location mLocation;
    private Marker mCurrLocationMarker;
    private ArrayList<Brewery> breweries = new ArrayList<>();
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private String uid;
    private Profile profile = new Profile("NA", 0, "NA", 0, null);
    public static final String FILE_NAME = "florida_brew.json";
    private HashMap<String, Marker> markers = new HashMap<String,Marker>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Firebase
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        uid = user.getUid();


        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // new user or sign in handled
        Intent intent = getIntent();
        int intExtra = intent.getIntExtra("newUser1234", 0);
        if (intExtra == 1) {
            Log.d(TAG, "onCreate: newUser: YES");
            createAProfile();
        } else {
            getProfileDataFromFirebase();
        }

        //checking permissions
        if (hasPermissions()) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                askPermission();
            }

            setContentView(R.layout.activity_beer_map);

            // Set the support map frag
            SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);

            Objects.requireNonNull(supportMapFragment).getMapAsync(this);

            // Begin the location updates
            startLocation();

            mLocation = Objects.requireNonNull(locationManager).getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        } else {
            // ask for permission
            askPermission();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Return from SelectedBreweryActivity
        if (requestCode == TO_SELECTED_BREWERY && resultCode == RESULT_OK) {
            if (data != null) {
                Serializable brewery = data.getSerializableExtra("BREWERY_RETURN");
                if (brewery instanceof Brewery) {

                    // Saves the updated brewery in arraylist
                    for (int i = 0; i < breweries.size(); i++) {
                        if (breweries.get(i).getName().equals(((Brewery) brewery).getName())) {
                            breweries.set(i, (Brewery) brewery);
                        }
                       saveProfile();
                    }
                }
            }
        }
        // Return from Profile and segue to SelectedBreweryActivity to edit beer
        if (requestCode == TO_PROFILE && resultCode == RESULT_OK) {
            if (data != null) {
                Serializable beer = data.getSerializableExtra("SELECTED_BEER");
                if (beer instanceof BeersTried) {
                    for (int i = 0; i < breweries.size(); i ++) {
                        if (breweries.get(i).getName().equals(((BeersTried) beer).getBreweryFrom())) {
                        Intent intent = new Intent(this, SelectedBreweryActivity.class);
                        intent.putExtra("SELEC_BREWERY", breweries.get(i));
                        intent.putExtra("SELEC_BEER", beer);
                        startActivityForResult(intent, TO_SELECTED_BREWERY);
                    }
                }
            }}
        }
    }

    // Handles search bar and profile menu button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.brewery_map_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint("Search Breweries");
        searchView.setOnQueryTextListener(this);
        searchView.setIconified(true);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (markers.containsKey(query)) {
                    String fixedQuery = query.toLowerCase().trim();
                    Marker marker = markers.get(fixedQuery);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 10));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    // Segue to UserProfileActivity
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.profile) {
            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.putExtra("PROFILE", profile);
            startActivityForResult(intent, TO_PROFILE);
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d(TAG, "onQueryTextSubmit: " + query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d(TAG, "onQueryTextChange: " + newText);
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            askPermission();
            return;
        }
        mLocation = Objects.requireNonNull(locationManager).getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (mLocation == null) {
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    10000,
                    0.3f,
                    this);


            mLocation = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (mLocation != null) {
                LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());




                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
            }
        }
    }

    // Handles users current location
    @Override
    public void onLocationChanged(Location location) {

        mLocation = location;

        if (mLocation != null) {

            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }

            LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());


            try {
                getCityAndExecuteDataTask(latLng);
            } catch (IOException e) {
                e.printStackTrace();
            }

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocationMarker = mMap.addMarker(markerOptions);

            // Zooms into current location
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        startLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
        stopLocation();
    }

    // Segue to SelectedBreweryActivity
    @Override
    public void onInfoWindowClick(Marker marker) {
        String name = marker.getTitle();
        for (int i = 0; i < breweries.size(); i ++) {
            if (breweries.get(i).getName().equals(name)) {
                Log.d(TAG, "onInfoWindowClick: " + breweries.size());
                Intent intent = new Intent(this, SelectedBreweryActivity.class);
                intent.putExtra("BREWERY", breweries.get(i));
                startActivityForResult(intent, TO_SELECTED_BREWERY);
            }
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @SuppressWarnings({"MissingPermission"})
    private void startLocation(){

        LocationManager locationManager= (LocationManager)getSystemService(LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    10000,
                    0.3f,
                    this );
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private void stopLocation(){
        LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.removeUpdates(BeerMapActivity.this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSIONS && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                askPermission();
            }
            setContentView(R.layout.activity_beer_map);
            // Update the map fragment
            SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            Objects.requireNonNull(supportMapFragment).getMapAsync(this);

            //starting location updates
            startLocation();
            mLocation = Objects.requireNonNull(locationManager).getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                boolean yes = shouldShowRequestPermissionRationale(permissions[0]);
                if (!yes) {
                    new AlertDialog.Builder(this)
                            .setTitle("Permissions Were Denied")
                            .setMessage("Please allow this app to access your location if you wish to use it.")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    askPermission();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setCancelable(false)
                            .show();

                }
                else {
                    Toast.makeText(this, "you must have location permission to use this app.", Toast.LENGTH_SHORT)
                    .show();
                    askPermission();
                }
            }
        }
    }

    private Boolean hasPermissions(){
        return ContextCompat.checkSelfPermission(BeerMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void askPermission(){
        if(!hasPermissions()) {
            ActivityCompat.requestPermissions(BeerMapActivity.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSIONS);
        } else {
            finish();
            startActivity(getIntent());
        }
    }



    public void getCityAndExecuteDataTask(LatLng latLng) throws IOException {

        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = gcd.getFromLocation(latLng.latitude, latLng.longitude, 1);
        if (addresses.size() > 0) {
            System.out.println(addresses.get(0).getLocality());
            Log.d(TAG, "getCityName: " + addresses.get(0).getPostalCode());

            // Temp position for method to pull from json file florida_brew.json
            pullDataFromFile();

            if (checkConnection()) {
                DataTask dataTask = new DataTask();
                Log.d(TAG, "getCityName: Calling DataTask");
                // When this apis server works again, this is the way the app is supposed to function
                //dataTask.execute("https://api.openbrewerydb.org/breweries?by_state=" + addresses.get(0).getAdminArea());
            } else {
                Log.d(TAG, "getCityName: NO CONNECTION");
            }
        }
        else {
            Log.d(TAG, "getCityName: Addresses size is larger than 0");
        }
    }

    // Checks if can connect to internet, if not, then internal storage/Toasts are handled here.
    private boolean checkConnection() {
        ConnectivityManager mgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if (mgr != null) {
            NetworkInfo info = mgr.getActiveNetworkInfo();
            if (info != null) {
                return true;
            } else {
                Log.i(TAG, "checkConnection: NetworkInfo is null");
            }
        } else {
            Log.i(TAG, "checkConnection: ConnectivityManager is null");
        }
        return false;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View contents = LayoutInflater.from(this)
                .inflate(R.layout.marker_info_window, null);

        ((TextView)contents.findViewById(R.id.title)).setText(marker.getTitle());
        ((TextView)contents.findViewById(R.id.distance)).setText(marker.getSnippet());

        return contents;
    }

    // Temp solution to my api link not working. This pulls from the json file in assets folder
    private void pullDataFromFile() throws IOException {
        String json = null;
        InputStream is = getApplicationContext().getAssets().open(FILE_NAME);

        int size = is.available();

        byte[] buffer = new byte[size];

        is.read(buffer);

        is.close();

        json = new String(buffer, "UTF-8");

        try {
            Log.d(TAG, "onPostExecute: Made it here");
            JSONArray outerMostObject = new JSONArray(json);
            //////////////breweries.clear();
            for (int i = 0; i < outerMostObject.length(); i++) {
                JSONObject brewery = outerMostObject.getJSONObject(i);
                int breweryId = brewery.getInt("id");
                String name = brewery.getString("name");
                String lat = brewery.getString("latitude");
                String lon = brewery.getString("longitude");
                String phone = brewery.getString("phone");
                String url = brewery.getString("website_url");

                Log.d(TAG, "onPostExecute: " + name);
                Brewery brew = null;

                if (lat.equals("null") || lon.equals("null")) {
                    String street = brewery.getString("street");
                    String city = brewery.getString("city");
                    String state = brewery.getString("state");
                    String zip = brewery.getString("postal_code");

                    String strAddress = street + ", " + city + ", " + state + " " + zip;

                    Geocoder coder = new Geocoder(getApplicationContext());
                    List<Address> address;

                    try {
                        // May throw an IOException
                        address = coder.getFromLocationName(strAddress, 5);
                        if (address == null) {
                            Log.d(TAG, "onPostExecute: Null address");
                        }

                        Address location = address.get(0);

                        float[] results = new float[1];

                        Location.distanceBetween(
                                location.getLatitude(),
                                location.getLongitude(),
                                mLocation.getLatitude(),
                                mLocation.getLongitude(),
                                results);

                        double milesDistance = results[0] * 0.000621371192;

                        // TODO: Creation of new ArrayList might be a problem

                        ArrayList<BeersTried> beersTrieds = new ArrayList<>();

                        brew = new Brewery(breweryId, name, location.getLatitude(),
                                location.getLongitude(), phone, url,
                                Math.round(milesDistance * 10) / 10.0, 0, "Never Visited", 0,beersTrieds);

                        boolean alreadyObtained = false;

                        for (int z = 0; z < breweries.size(); z++) {
                            if (breweries.get(z).getName().equals(name)) {
                                alreadyObtained = true;
                            }
                        }
                        if (!alreadyObtained) {
                            breweries.add(brew);
                        }

                    } catch (IOException ex) {

                        ex.printStackTrace();
                    }
                } else {
                    float[] results = new float[1];

                    Location.distanceBetween(
                            Double.parseDouble(lat),
                            Double.parseDouble(lon),
                            mLocation.getLatitude(),
                            mLocation.getLongitude(),
                            results);

                    double milesDistance = results[0] * 0.000621371192;


                    boolean alreadyObtained = false;

                    for (int z = 0; z < breweries.size(); z++) {
                        if (breweries.get(z).getName().equals(name)) {
                            alreadyObtained = true;
                        }
                    }

                    brew = new Brewery(breweryId, name, Double.parseDouble(lat), Double.parseDouble(lon), phone, url, Math.round(milesDistance * 10) / 10.0,
                            0,"Never Visited",0,null);
                    if (!alreadyObtained) {
                        breweries.add(brew);
                    }
                }

                if (brew != null) {



                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(brew.getLatLng());
                    markerOptions.title(name);
                    markerOptions.snippet("~ " + brew.getDistance() + "m");
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    // Add marker to marker map
                    Marker marker = mMap.addMarker(markerOptions);

                    // Add marker to collection for search bar
                    if (!markers.containsKey(name)) {
                        markers.put(name.toLowerCase().trim(), marker);
                    }
                }


            }

        } catch (JSONException e) {
            Log.i(TAG, "onPostExecute: " + e);
        }
        // load brewery data from firebase
        getBreweryDataFromFirebase();

    }

    // Pulls JSON data
    private class DataTask extends AsyncTask<String, Void, String> {

        // Tests connection and pulls data from api url
        @Override
        protected String doInBackground(String... params) {
            try {
                Log.d(TAG, "doInBackground: Made it here");
                URL url = new URL(params[0]);
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                connection.connect();

                InputStream is = connection.getInputStream();
                String data = IOUtils.toString(is);
                is.close();
                connection.disconnect();

                return data;
            } catch (IOException e) {
                Log.i(TAG, "doInBackground: " + e);
            }

            Log.i(TAG, "doInBackground: doInBackground returned null value");
            return null;
        }

        // Json pull and posts addition.
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.d(TAG, "onPostExecute: Made it here");
                JSONArray outerMostObject = new JSONArray(s);
                //////////////breweries.clear();
                for (int i = 0; i < outerMostObject.length(); i++) {
                    JSONObject brewery = outerMostObject.getJSONObject(i);
                    int breweryId = brewery.getInt("id");
                    String name = brewery.getString("name");
                    String lat = brewery.getString("latitude");
                    String lon = brewery.getString("longitude");
                    String phone = brewery.getString("phone");
                    String url = brewery.getString("website_url");

                    Log.d(TAG, "onPostExecute: " + name);
                    Brewery brew = null;

                    if (lat.equals("null") || lon.equals("null")) {
                        String street = brewery.getString("street");
                        String city = brewery.getString("city");
                        String state = brewery.getString("state");
                        String zip = brewery.getString("postal_code");

                        String strAddress = street + ", " + city + ", " + state + " " + zip;

                        Geocoder coder = new Geocoder(getApplicationContext());
                        List<Address> address;

                        try {
                            // May throw an IOException
                            address = coder.getFromLocationName(strAddress, 5);
                            if (address == null) {
                                Log.d(TAG, "onPostExecute: Null address");
                            }

                            Address location = address.get(0);

                            float[] results = new float[1];

                            Location.distanceBetween(
                                    location.getLatitude(),
                                    location.getLongitude(),
                                    mLocation.getLatitude(),
                                    mLocation.getLongitude(),
                                    results);

                            double milesDistance = results[0] * 0.000621371192;

                            // TODO: Creation of new ArrayList might be a problem

                            ArrayList<BeersTried> beersTrieds = new ArrayList<>();

                            brew = new Brewery(breweryId, name, location.getLatitude(),
                                    location.getLongitude(), phone, url,
                                    Math.round(milesDistance * 10) / 10.0, 0, "Never Visited", 0,beersTrieds);

                            boolean alreadyObtained = false;

                            for (int z = 0; z < breweries.size(); z++) {
                                if (breweries.get(z).getName().equals(name)) {
                                    alreadyObtained = true;
                                }
                            }
                            if (!alreadyObtained) {
                                breweries.add(brew);
                            }

                        } catch (IOException ex) {

                            ex.printStackTrace();
                        }
                    } else {
                        float[] results = new float[1];

                        Location.distanceBetween(
                                Double.parseDouble(lat),
                                Double.parseDouble(lon),
                                mLocation.getLatitude(),
                                mLocation.getLongitude(),
                                results);

                        double milesDistance = results[0] * 0.000621371192;



                        brew = new Brewery(breweryId, name, Double.parseDouble(lat), Double.parseDouble(lon), phone, url, Math.round(milesDistance * 10) / 10.0,
                                0,"Never Visited",0,null);
                        breweries.add(brew);
                    }

                    if (brew != null) {


                        // Add new Marker
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(brew.getLatLng());
                        markerOptions.title(name);
                        markerOptions.snippet("~ " + brew.getDistance() + "m");
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        mMap.addMarker(markerOptions);

                        // Add marker to marker map
                        Marker marker = mMap.addMarker(markerOptions);

                        // Add marker to collection for search bar
                        if (!markers.containsKey(name)) {
                            markers.put(name.toLowerCase().trim(), marker);
                        }
                    }


                }

            } catch (JSONException e) {
                Log.i(TAG, "onPostExecute: " + e);
            }

            // load brewery data from firebase
            getBreweryDataFromFirebase();

        }

    }


    // Creates the very post to the firebase.
    private void createAProfile() {

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
        String formattedDate = df.format(c);

        profile = new Profile(formattedDate, 0, "NA", 0, null);


            myRef = database.getReference("users/" + uid);
            Map<String, Object> userData = new HashMap<>();
            userData.put("creationDate", profile.getCreationDate());
            userData.put("breweriesVisited", profile.getBreweriesVisited());
            userData.put("mostVisited", profile.getMostVisited());
            userData.put("moneySpent", profile.getMoneySpent());
            myRef = myRef.child("UserProfile");
            myRef.setValue(userData);

    }

    // Saves profile data to the firebase
    private void saveProfile() {

        getProfileDataFromFirebase();

            String favBrewery = "NA";
            int amountHolder = 0;
            int totalCountVisited = 0;
            double moneyCount = 0;
            ArrayList<BeersTried> allBeers = new ArrayList<>();
            for (int i = 0; i < breweries.size(); i++) {
                if (breweries.get(i).getTimesVisited() > amountHolder) {
                    favBrewery = breweries.get(i).getName();
                    amountHolder = breweries.get(i).getTimesVisited();
                }
                if (breweries.get(i).getTimesVisited() > 0) {
                    totalCountVisited += 1;
                }

                if (breweries.get(i).getBeers() != null) {
                    moneyCount += breweries.get(i).getTotalMoneySpent();
                    allBeers.addAll(breweries.get(i).getBeers());
                }

            }

            profile.setMostVisited(favBrewery);
            profile.setBreweriesVisited(totalCountVisited);
            profile.setMoneySpent(moneyCount);
            profile.setFavoriteBeers(allBeers);

            myRef = database.getReference("users/" + uid);
            Map<String, Object> userData = new HashMap<>();
            userData.put("creationDate", profile.getCreationDate());
            userData.put("breweriesVisited", profile.getBreweriesVisited());
            userData.put("mostVisited", profile.getMostVisited());
            userData.put("moneySpent", profile.getMoneySpent());
            myRef = myRef.child("UserProfile");
            myRef.setValue(userData);

            if (profile.getFavoriteBeers() != null) {

                for (int i = 0; i < profile.getFavoriteBeers().size(); i++) {
                    myRef = database.getReference("users/" + uid + "/UserProfile/FavoriteBeers");
                    BeersTried beersTried = profile.getFavoriteBeers().get(i);

                    Map<String, Object> userData2 = new HashMap<>();
                    userData2.put("id", beersTried.getId());
                    userData2.put("name", beersTried.getName());
                    userData2.put("rating", beersTried.getRating());
                    userData2.put("cost", beersTried.getCost());
                    userData2.put("abv", beersTried.getAbv());
                    userData2.put("lastDrank", beersTried.getLastDrank());
                    userData2.put("notes", beersTried.getNotes());
                    userData2.put("amountDrank", beersTried.getAmountDrank());
                    userData2.put("breweryFrom", beersTried.getBreweryFrom());
                    myRef = myRef.child(beersTried.getName());
                    myRef.setValue(userData2);
                }
            }

    }

    // This is called when profile data is needed from firebase
    private void getProfileDataFromFirebase() {
        myRef = FirebaseDatabase.getInstance().getReference("users/" + uid);
        myRef.child("UserProfile").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                        profile = dataSnapshot.getValue(Profile.class);
                        myRef = FirebaseDatabase.getInstance().getReference("users/" + uid + "/UserProfile");
                        myRef.child("FavoriteBeers").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                Iterable<DataSnapshot> contactChildren = snapshot.getChildren();
                                ArrayList<BeersTried> beers = new ArrayList<>();
                                for (DataSnapshot contact : contactChildren) {
                                    BeersTried c = contact.getValue(BeersTried.class);
                                    beers.add(c);
                                }
                                profile.setFavoriteBeers(beers);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    // This is called when brewery data is needed from firebase
    private void getBreweryDataFromFirebase() {
        myRef = FirebaseDatabase.getInstance().getReference("users/" + uid);
        myRef.child( "Breweries Visited").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Log.d(TAG, "onDataChange: " + dataSnapshot);
                    Iterable<DataSnapshot> contactChildren = dataSnapshot.getChildren();
                    ArrayList<Brewery> brew = new ArrayList<>();

                    for (DataSnapshot contact : contactChildren) {
                        Brewery c = contact.getValue(Brewery.class);
                        Log.d("contact:: ", c.getName() + " " + c.getPhone());
                        brew.add(c);

                        for (int i = 0; i < breweries.size(); i++) {
                            if (breweries.get(i).getName().equals(c.getName())) {
                                breweries.get(i).setTimesVisited(c.getTimesVisited());
                                breweries.get(i).setLastVisited(c.getLastVisited());
                                breweries.get(i).setRating(c.getRating());
                                getBeerDataFromFirebase(breweries.get(i).getName());
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    // This is called when beer data is needed from firebase
    private void getBeerDataFromFirebase(final String brewery) {
        myRef = FirebaseDatabase.getInstance().getReference("users/" + uid + "/Breweries Visited/" + brewery );
        myRef.child("Favorite Beers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    Log.d(TAG, "onDataChange: " + dataSnapshot);
                    Iterable<DataSnapshot> contactChildren = dataSnapshot.getChildren();
                    ArrayList<BeersTried> beers = new ArrayList<>();

                    for (DataSnapshot contact : contactChildren) {
                        BeersTried c = contact.getValue(BeersTried.class);
                        Log.d("contact:: ", c.getName() + " " + c.getRating());
                        beers.add(c);
                    }
                    for (int i = 0; i < breweries.size(); i++) {
                        if (breweries.get(i).getName().equals(brewery)) {
                            breweries.get(i).setBeers(beers);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}