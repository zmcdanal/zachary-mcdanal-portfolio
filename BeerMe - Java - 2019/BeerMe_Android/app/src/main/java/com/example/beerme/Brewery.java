package com.example.beerme;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

public class Brewery implements Serializable {

    // Declarations
    private int id;
    private String name;
    private double lat;
    private double lon;
    private String phone;
    private String url;
    private double distance;
    private int timesVisited;
    private String lastVisited;
    private int rating;
    private ArrayList<BeersTried> beers;

    public Brewery() {

    }

    // Constructor
    public Brewery(int id, String name, double lat, double lon, String phone, String url,
                   double distance, int timesVisited, String lastVisited, int rating, ArrayList<BeersTried> beers) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.phone = phone;
        this.url = url;
        this.distance = distance;
        this.timesVisited = timesVisited;
        this.lastVisited = lastVisited;
        this.rating = rating;
        this.beers = beers;
    }


    // Getters
    public int getTimesVisited() {
        return timesVisited;
    }

    public int getId() {
        return id;
    }

    public String getStringTimesVisited() {
        return String.valueOf(timesVisited);
    }

    public String getLastVisited() {
        return lastVisited;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public LatLng getLatLng() {
        return new LatLng(getLat(), getLon());
    }

    public String getPhone() {
        return phone;
    }

    public String getUrl() {
        return url;
    }

    public double getDistance() {
        return distance;
    }

    public int getRating() {
        return rating;
    }

    public ArrayList<BeersTried> getBeers() {
        return beers;
    }

    public double getTotalMoneySpent() {
        int cost = 0;
        for (int i = 0; i < beers.size(); i ++) {
            cost += (beers.get(i).getCost() * beers.get(i).getAmountDrank());
        }
        return cost;
    }

    // Setters
    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTimesVisited(int timesVisited) {
        this.timesVisited = timesVisited;
    }

    public void setLastVisited(String lastVisited) {
        this.lastVisited = lastVisited;
    }

    public void setBeers(ArrayList<BeersTried> beers) {
        this.beers = beers;
    }
}
