package com.example.beerme;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class LoginFragment extends Fragment {

    // TAG
    private static final String TAG = "LoginFragment";

    // Listener
    private Listener mListener;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {

        Bundle args = new Bundle();

        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof Listener) {
            mListener = (Listener) context;
        }
    }

    // Interface
    public interface Listener {
        void signIn(String email, String password);
        void createAccount();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {
            ImageButton loginImgBtn = getView().findViewById(R.id.loginImgBtn);
            TextView createAccountTV = getView().findViewById(R.id.createAccountTVBtn);

            loginImgBtn.setOnClickListener(mClickListener);

            createAccountTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.createAccount();
                }
            });



        }
    }

    // OnClickListener for the loginImgBtn
    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getView() != null) {
                    EditText emailET = getView().findViewById(R.id.emailET);
                    EditText passwordET = getView().findViewById(R.id.passwordET);

                    String email = emailET.getText().toString().trim();
                    String password = passwordET.getText().toString().trim();

                    // Check to make sure EditTexts are not blank
                    if (!email.equals("") && !password.equals("")) {
                        // Listener is fired
                        mListener.signIn(email, password);

                    } else {
                        Toast.makeText(getContext(), "Please do not leave anything blank", Toast.LENGTH_SHORT)
                                .show();
                    }
            }
        }
    };


}
