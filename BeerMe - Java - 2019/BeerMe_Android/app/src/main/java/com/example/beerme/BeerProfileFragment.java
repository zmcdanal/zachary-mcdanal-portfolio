package com.example.beerme;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BeerProfileFragment extends Fragment {

    // Various Variables
    private static final String TAG = "BeerProfileFragment";
    private BeersTried beersTried;
    private Listener mListener;

    public BeerProfileFragment() {
    }

    public static BeerProfileFragment newInstance(BeersTried beersTried) {

        Bundle args = new Bundle();
        args.putSerializable(TAG, beersTried);
        BeerProfileFragment fragment = new BeerProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof BeerProfileFragment.Listener) {
            mListener = (BeerProfileFragment.Listener) context;
        }
    }

    // Interface
    public interface Listener {
        void saveBeer(BeersTried beersTried);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_beer_profile, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null && getView() != null) {
            beersTried = (BeersTried) getArguments().getSerializable(TAG);

            if (beersTried != null) {

                // findViewByIds
                TextView titleTV = getView().findViewById(R.id.beerNameTV);
                TextView abvTV = getView().findViewById(R.id.abvTV);
                TextView drinkTV = getView().findViewById(R.id.drinkPriceTV);
                TextView drankTV = getView().findViewById(R.id.drankDateTV);
                TextView drunkTV = getView().findViewById(R.id.drunkCountTV);
                TextView notesTV = getView().findViewById(R.id.notesTV);

                ImageView star1IV = getView().findViewById(R.id.star1Details);
                ImageView star2IV = getView().findViewById(R.id.star2Details);
                ImageView star3IV = getView().findViewById(R.id.star3Details);
                ImageView star4IV = getView().findViewById(R.id.star4Details);
                ImageView star5IV = getView().findViewById(R.id.star5Details);

                String fullPriceSetup = "$" + beersTried.getStringCost();
                String fullABVSetup = beersTried.getStringABV() + "%";

                // setting texts on page
                titleTV.setText(beersTried.getName());
                abvTV.setText(fullABVSetup);
                drinkTV.setText(fullPriceSetup);
                drankTV.setText(beersTried.getLastDrank());
                drunkTV.setText(beersTried.getStringAmountDrank());
                notesTV.setText(beersTried.getNotes());


                // Ranking check
                if (beersTried.getRating() >= 1) {
                    star1IV.setImageResource(R.drawable.star);
                } else {
                    star1IV.setImageResource(R.drawable.emptystar);
                }
                if (beersTried.getRating() >= 2) {
                    star2IV.setImageResource(R.drawable.star);
                } else {
                    star2IV.setImageResource(R.drawable.emptystar);
                }
                if (beersTried.getRating() >= 3) {
                    star3IV.setImageResource(R.drawable.star);
                } else {
                    star3IV.setImageResource(R.drawable.emptystar);
                }
                if (beersTried.getRating() >= 4) {
                    star4IV.setImageResource(R.drawable.star);
                } else {
                    star4IV.setImageResource(R.drawable.emptystar);
                }
                if (beersTried.getRating() == 5) {
                    star5IV.setImageResource(R.drawable.star);
                } else {
                    star5IV.setImageResource(R.drawable.emptystar);
                }

                // saves the beer
                ImageButton editBtn = getView().findViewById(R.id.saveAndReturn);
                editBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.saveBeer(beersTried);
                    }
                });

            }
        }
    }
}
