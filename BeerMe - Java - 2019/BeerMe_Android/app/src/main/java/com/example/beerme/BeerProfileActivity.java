package com.example.beerme;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BeerProfileActivity extends AppCompatActivity implements BeerProfileFragment.Listener {

    // Various Variables
    private static final String TAG = "BeerProfileActivity";
    private BeersTried beerTried;
    public static final int EDIT_BEER = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beer_profile);

        Intent intent = getIntent();
        Serializable beer = intent.getSerializableExtra("BEER");
        if (beer instanceof BeersTried) {
            beerTried = (BeersTried) beer;
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_beerProfile_container, BeerProfileFragment.newInstance((BeersTried) beer))
                    .commit();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Return from editing beer
        if (requestCode == EDIT_BEER && resultCode == RESULT_OK) {
            if (data != null) {
                Serializable beer = data.getSerializableExtra("com.example.beerMe.EDITBEER");
                if (beer instanceof BeersTried) {
                    beerTried = (BeersTried) beer;
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_beerProfile_container, BeerProfileFragment.newInstance((BeersTried) beer))
                            .commit();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.beer_profile_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // TODO: This should increase the count of beers drank. Maybe update to database

        // Return with an extra beer
        if (item.getItemId() == R.id.downTheHatch) {
            int count = Integer.parseInt(beerTried.getStringAmountDrank());
            int newCount = count + 1;
            beerTried.setAmountDrank(newCount);

            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
            String formattedDate = df.format(c);
            beerTried.setLastDrank(formattedDate);

            Intent returnIntent = new Intent();
            returnIntent.putExtra("com.example.beerMe.BEERUPDATE", beerTried);
            setResult(RESULT_OK, returnIntent);
            finish();
        }
        else if (item.getItemId() == R.id.edit) {
            Intent editIntent = new Intent(this, AddBeerActivity.class);
            editIntent.putExtra("BEERTOCHANGE", beerTried);
            startActivityForResult(editIntent, EDIT_BEER);
        }
        return true;
    }

    // saves the beer
    @Override
    public void saveBeer(BeersTried beersTried) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("com.example.beerMe.BEERUPDATE", beerTried);
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}