//
//  BOW_Data.swift
//  Solace_IOS
//
//  Created by Zakarias McDanal on 9/26/20.
//  Copyright © 2020 Jerad Alexander. All rights reserved.
//

import Foundation

class BOW_Data : Encodable, Decodable {
    var bookTitle: String
    var bookDesc: String
    var bookThumb: String
    var bookLink: String
    var timestamp: Int
    
    init(BookTitle: String, BookDesc: String, BookThumb: String, BookLink: String, Timestamp: Int) {
        self.bookTitle = BookTitle
        self.bookDesc = BookDesc
        self.bookThumb = BookThumb
        self.bookLink = BookLink
        self.timestamp = Timestamp
    }
    
//    required convenience init(coder aDecoder: NSCoder) {
//        let title = aDecoder.decodeObject(forKey: "title") as! String
//        let desc = aDecoder.decodeObject(forKey: "desc") as! String
//        let thumb = aDecoder.decodeObject(forKey: "thumb") as! String
//        let link = aDecoder.decodeObject(forKey: "link") as! String
//        let timestamp = aDecoder.decodeInteger(forKey: "timestamp")
//        self.init(BookTitle: title, BookDesc: desc, BookThumb: thumb, BookLink: link, Timestamp: timestamp)
//    }
//
//    func encode(with aCoder: NSCoder) {
//        aCoder.encode(bookTitle, forKey: "title")
//        aCoder.encode(bookDesc, forKey: "desc")
//        aCoder.encode(bookThumb, forKey: "thumb")
//        aCoder.encode(bookLink, forKey: "link")
//        aCoder.encode(timestamp, forKey: "timestamp")
//    }
}
