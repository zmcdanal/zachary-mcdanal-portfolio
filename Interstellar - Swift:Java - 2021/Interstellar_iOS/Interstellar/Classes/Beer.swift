//
//  Beer.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/6/20.
//

import Foundation
import MapKit
import Contacts

class Beer {
    var title: String
    var photo: String
    var descrip: String
    var abv: String
    var ingredients: [String]
    var locations: [MapLocation]
    
    init(Title: String, Photo: String, Descrip: String, Abv: String, Ingredients: [String], Locations: [MapLocation]) {
        self.title = Title
        self.photo = Photo
        self.descrip = Descrip
        self.abv = Abv
        self.ingredients = Ingredients
        self.locations = Locations
    }
    
    var beerIngredients: String {
        var itemsToReturn = ""
        for item in ingredients {
            itemsToReturn += "• \(item)\n"
        }
        return itemsToReturn
    }
}



