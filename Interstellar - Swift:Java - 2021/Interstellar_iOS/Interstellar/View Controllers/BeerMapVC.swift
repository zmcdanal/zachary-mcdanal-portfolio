//
//  BeerMapVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/5/20.
//

import UIKit
import MapKit

class BeerMapVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var locatingLbl: UILabel!
    
    // Various Variables
    var beer: Beer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        mapView.delegate = self
        mapView.isZoomEnabled = true
        // Do any additional setup after loading the view.
        
        // Set initial location in general brewery location
        let initialLocation = CLLocation(latitude: 33.1876830, longitude: -86.7818640)

        mapView.centerToLocation(initialLocation, regionRadius: 400000)
        mapView.addAnnotations(beer!.locations)
        
        // Hint label setup
        let textToShow = "Zoom in to see more!\nNote: It is our recommendation to call ahead to check stock... "
        locatingLbl.text = textToShow
    }
    
    // Setup for the top navigation bar
    func setupNavBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#f8ddB0")
        self.navigationController?.navigationBar.tintColor = UIColor(hexString: "#CA4D3C")
        
        let customImageBarBtn1 = UIBarButtonItem(
            image: UIImage(named: "dismiss.png")!.withRenderingMode(.alwaysOriginal),
            style: .plain, target: self, action: #selector(dismissPage(_:)))
        navigationItem.leftBarButtonItem = customImageBarBtn1
    }
    
    // Method called from top nav bar that dismisses page
    @objc func dismissPage(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Before page dismisses, change button background on DashVC from red back to default color
    // Also, clears the annotations from mapView to get ready for next use
    override func viewWillDisappear(_ animated: Bool) {
        if let presenter = presentingViewController as? DashVC {
            presenter.squares[1].backgroundColor = UIColor(hexString: "#F8DDB0")
            presenter.buttons[1].setTitleColor(UIColor(hexString: "#F8DDB0"), for: .normal)
            presenter.selectionViews[1].isHidden = true
        }
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
    }
    
}

// Further MapView setup
extension BeerMapVC: MKMapViewDelegate {
    
  func mapView(
    _ mapView: MKMapView,
    viewFor annotation: MKAnnotation
  ) -> MKAnnotationView? {
    guard let annotation = annotation as? MapLocation else {
      return nil
    }
    let identifier = "beerLocation"
    var view: MKMarkerAnnotationView
    
    if let dequeuedView = mapView.dequeueReusableAnnotationView(
      withIdentifier: identifier) as? MKMarkerAnnotationView {
      dequeuedView.annotation = annotation
      view = dequeuedView
    } else {
      view = MKMarkerAnnotationView(
        annotation: annotation,
        reuseIdentifier: identifier)
      view.canShowCallout = true
      view.calloutOffset = CGPoint(x: -5, y: 5)
      view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    return view
  }
    
    func mapView(
      _ mapView: MKMapView,
      annotationView view: MKAnnotationView,
      calloutAccessoryControlTapped control: UIControl
    ) {
      guard let mapLocation = view.annotation as? MapLocation else {
        return
      }

      let launchOptions = [
        MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
      ]
      mapLocation.mapItem?.openInMaps(launchOptions: launchOptions)
    }
}
