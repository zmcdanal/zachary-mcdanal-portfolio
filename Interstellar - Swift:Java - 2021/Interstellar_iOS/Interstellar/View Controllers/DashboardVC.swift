//
//  DashboardVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/5/20.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
     
    @IBOutlet weak var mapView: MKMapView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.addBackground()
        
        //mapView.delegate = self
        
        // Set to new colour for whole app
        UINavigationBar.appearance().barTintColor = UIColor(hexString: "#f8ddB0")
        
        // Set initial location in Honolulu
        let initialLocation = CLLocation(latitude: 33.1876830, longitude: -86.7818640)

        mapView.centerToLocation(initialLocation)
        
        let artwork = MapLocation(
          title: "Interstellar Ginger Beer & Exploration Co",
          locationName: "260A Regency Park Dr, Alabaster, AL 35007",
          discipline: "Sculpture",
          coordinate: CLLocationCoordinate2D(latitude: 33.1876830, longitude: -86.7818640))
        mapView.addAnnotation(artwork)

    }
    
    func setMapFocus(){
    }
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            let vc = storyboard!.instantiateViewController(withIdentifier: "AboutUs") as! AboutUsVC
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
            break
        case 1:
            let vc = storyboard!.instantiateViewController(withIdentifier: "OurBeers") as! OurBeersVC
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
            break
        case 2:
            break
        case 4:
            break
        default:
            print("Dashboard: Error with buttonClicked() tag")
        }
    }
    
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 1000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}
