//
//  OurBeersVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/3/20.
//

import UIKit
import CoreLocation

class OurBeersVC: UIViewController, UIScrollViewDelegate {
    
    // Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var slides = [BeerView]()
    
    // Various Variables
    var allBeers = [Beer]()
    var beerLocations = [MapLocation]()
    var imageArray = [UIImage]()
    var beerCount = 0
    var indexForBeerMap = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addBackground(which: 0)
        
        // Do any additional setup after loading the view.
        imageArray += [UIImage(named: "firstcontact")!, UIImage(named: "spacemule")!, UIImage(named: "gingercolada")!, UIImage(named: "starberry")!,
                       UIImage(named: "andromeda")!, UIImage(named: "memberries")!, UIImage(named: "melons")!, UIImage(named: "mojito")!, UIImage(named: "darkstormy")!,
                       UIImage(named: "peachme")!, UIImage(named: "teq")!, UIImage(named: "borntorum")!, UIImage(named: "seltzer")!, UIImage(named: "social")!]
        
        // setupImages(imageArray)
        //scrollViewDidScroll(beerScroll)
        
        
        setupNavBar()
        
        allBeers.removeAll()
        readJsonData()
        print(allBeers.count)
        
        scrollView.delegate = self
        createSlides()
        
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    func createSlides() {
        for i in 0..<allBeers.count {
            let slide: BeerView = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! BeerView
            slide.beerIV.image = UIImage(named: allBeers[i].photo)
            slide.beerTitleLbl.text = allBeers[i].title
            slide.beerDescriptionLbl.text = allBeers[i].descrip
            slide.abvLbl.text = allBeers[i].abv
            slide.ingredientsLbl.text = allBeers[i].beerIngredients
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openBeerMap(_:)))
            slide.locateBeerBtn.isUserInteractionEnabled = true
            slide.locateBeerBtn.addGestureRecognizer(tapGestureRecognizer)
            
            slides.append(slide)
        }
        setupSlideScrollView()
    }
    
    func setupSlideScrollView() {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: 0)
        scrollView.isPagingEnabled = true
        print(slides.count)
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
    
    // Setup for the top navigation bar
    func setupNavBar() {
        let image = UIImage(named: "logo")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#f8ddB0")
        self.navigationController?.navigationBar.tintColor = UIColor(hexString: "#CA4D3C")
        
        navigationItem.titleView = imageView
        
        let customImageBarBtn1 = UIBarButtonItem(
            image: UIImage(named: "dismiss.png")!.withRenderingMode(.alwaysOriginal),
            style: .plain, target: self, action: #selector(dismissPage(_:)))
        navigationItem.leftBarButtonItem = customImageBarBtn1
    }
    
    // Method called from top nav bar that dismisses page
    @objc func dismissPage(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Before page dismisses, change button background on DashVC from red back to default color
    override func viewWillDisappear(_ animated: Bool) {
        if let presenter = presentingViewController as? DashVC {
            presenter.squares[1].backgroundColor = UIColor(hexString: "#F8DDB0")
            presenter.buttons[1].setTitleColor(UIColor(hexString: "#F8DDB0"), for: .normal)
            presenter.selectionViews[1].isHidden = true
        }
    }
    
    // Segue to BeerMapVC after "Locate Beer" on Nav bar is clicked
    @objc func openBeerMap(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "BeerMap", sender: self)
    }
    
    // Prepares for segue to BeerMapVC by taking current beer being displayed with us
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? BeerMapVC {
            destinationVC.beer = allBeers[self.pageControl.currentPage]
        }
    }
    
    
    // Reads data from BeerData.json
    func readJsonData() {
        //Get the path to our BeerData.json file
        if let path = Bundle.main.path(forResource: "BeerData", ofType: ".json") {
            
            //Create URL with path
            let url = URL(fileURLWithPath: path)
            
            do {
                
                let data = try Data.init(contentsOf: url)
                
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                
                guard let json = jsonObj
                else {print("Parse Failed to Unwrap the Optional"); return}
                
                //Loop through 1st level objects
                for firstLevelItem in json {
                    //Try to convert 1st level object into a [String: Any]
                    guard let object = firstLevelItem as? [String: Any],
                          //Get the value of our properties for the current object
                          let beers = object["beers"] as? [[String: Any]]
                    
                    else { continue }
                    
                    for beer in beers {
                        let name = beer["name"] as! String
                        let photo = beer["photo"] as! String
                        let descrip = beer["descrip"] as! String
                        let abv = beer["abv"] as! String
                        let ingredients = beer["ingredients"] as! [String]
                        let locations = beer["locations"] as! [[String: Any]]
                        
                        var ingreds = [String]()
                        for item in ingredients {
                            ingreds.append(item)
                        }
                        
                        for point in locations {
                            let place = point["place"] as! String
                            let address = point["address"] as! String
                            let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .long, timeStyle: .none)
                            let inputFormatter = DateFormatter()
                            inputFormatter.dateFormat = "MMM dd, yyyy"
                            let showDate = inputFormatter.date(from: timestamp)
                            inputFormatter.dateFormat = "yyyy-MM-dd"
                            let resultTimestamp = inputFormatter.string(from: showDate!)
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd" //Your date format
                            dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
                            //according to date format your date string
                            guard let date = dateFormatter.date(from: resultTimestamp) else {
                                fatalError()
                            }
                            var prevDate = date
                            
                            if let lu = point["lastupdated"] as? String {
                                let inputFormatter = DateFormatter()
                                inputFormatter.dateFormat = "MMM dd, yyyy"
                                let showDate = inputFormatter.date(from: lu)
                                inputFormatter.dateFormat = "yyyy-MM-dd"
                                let lastUpdated = inputFormatter.string(from: showDate!)
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd" //Your date format
                                dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
                                //according to date format your date string
                                guard let newDate = dateFormatter.date(from: lastUpdated) else {
                                    fatalError()
                                }
                                prevDate = newDate
                            }
                            let now = Date()
                            let datePulled = prevDate
                            var dateComponent = DateComponents()
                            dateComponent.month = 5
                            let futureDate = Calendar.current.date(byAdding: dateComponent, to: datePulled)
                            
                            // Check to see if date is 5 months behind today's date
                            if (futureDate! >= now) {
                                 let lat = point["lat"] as! String
                                 let lon = point["lon"] as! String
                                 
                                 let coordinate = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(lon)!)
                                 // Adds new location to beerLocations array which will be used in the allBeers array
                                 let local = MapLocation(title: place, locationName: address, discipline: "Sculpture", coordinate: coordinate)
                                 self.beerLocations.append(local)
                            } else {
                                print("\(name) is no longer available at \(place): \(address), as of \(datePulled)")
                            }
                            
                           
                            
                        }
                        // Add new beer to allBeers array
                        let newBeer = Beer(Title: name, Photo: photo, Descrip: descrip, Abv: abv, Ingredients: ingreds, Locations: beerLocations)
                        allBeers.append(newBeer)
                        beerLocations.removeAll()
                    }
                }
                
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
    
}
