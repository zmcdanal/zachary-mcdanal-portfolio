//
//  PodcastsVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/5/20.
//

import UIKit

class PodcastsVC: UIViewController {

    // Outlets
    @IBOutlet weak var podcastIV: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addBackground(which: 1)
        setupNavBar()
        // Do any additional setup after loading the view.
        
        // Podcast ImageView tap gesture declaration
        let tap = UITapGestureRecognizer(target: self, action: #selector(PodcastsVC.openPodcast))
        podcastIV.addGestureRecognizer(tap)
        podcastIV.isUserInteractionEnabled = true
    }
    
    @objc func openPodcast(sender: UITapGestureRecognizer) {
        let podcastHooks = "https://podcasts.apple.com/us/podcast/the-ginger-beer-podcast/id1525236538"
        let podcastUrl = NSURL(string: podcastHooks)
        if UIKit.UIApplication.shared.canOpenURL(podcastUrl! as URL) {
            UIKit.UIApplication.shared.open(podcastUrl! as URL)
        } else {
            UIKit.UIApplication.shared.open(NSURL(string: "https://thegingerbeerpodcast.podbean.com")! as URL)
        }
    }
    
    // Setup for the top navigation bar
    func setupNavBar() {
        let image = UIImage(named: "logo")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit

        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#f8ddB0")
        self.navigationController?.navigationBar.tintColor = UIColor(hexString: "#CA4D3C")

        navigationItem.titleView = imageView
        let customImageBarBtn1 = UIBarButtonItem(
            image: UIImage(named: "dismiss.png")!.withRenderingMode(.alwaysOriginal),
            style: .plain, target: self, action: #selector(dismissPage(_:)))
        navigationItem.leftBarButtonItem = customImageBarBtn1
    }
    
    // Method called from top nav bar that dismisses page
    @objc func dismissPage(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Before page dismisses, change button background on DashVC from red back to default color
    override func viewWillDisappear(_ animated: Bool) {
        if let presenter = presentingViewController as? DashVC {
            presenter.squares[2].backgroundColor = UIColor(hexString: "#F8DDB0")
            presenter.buttons[2].setTitleColor(UIColor(hexString: "#F8DDB0"), for: .normal)
            presenter.selectionViews[2].isHidden = true
        }
    }
}
