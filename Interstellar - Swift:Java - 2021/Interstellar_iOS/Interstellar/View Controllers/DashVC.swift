//
//  DashVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/5/20.
//

import UIKit
import MapKit

class DashVC: UIViewController {
    
     // Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet var squares: [UIView]!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var selectionViews: [UIView]!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.addBackground(which: 0)
        
        // mapView setup
        mapView.delegate = self
        mapView.isZoomEnabled = true
        // Set initial location in general brewery location
        let initialLocation = CLLocation(latitude: 33.1876830, longitude: -86.7818640)
        mapView.centerToLocation(initialLocation, regionRadius: 15000)
        
        // Annotation of brewery
        let location = MapLocation(
          title: "Interstellar Ginger Beer & Exploration Co",
          locationName: "260A Regency Park Dr, Alabaster, AL 35007",
          discipline: "Sculpture",
          coordinate: CLLocationCoordinate2D(latitude: 33.1876830, longitude: -86.7818640))
        mapView.addAnnotation(location)
        
        // Set to new colour for whole app
        UINavigationBar.appearance().barTintColor = UIColor(hexString: "#f8ddB0")
    }
    
    // Handles navigation buttons on page
    @IBAction func buttonClicked(_ sender: UIButton) {
        switch sender.tag {
        case 0: // About Us
            squares[0].backgroundColor = UIColor(hexString: "#CA4D3C")
            buttons[0].setTitleColor(UIColor(hexString: "#CA4D3C"), for: .normal)
            selectionViews[0].isHidden = false
            
            let vc = storyboard!.instantiateViewController(withIdentifier: "AboutUs") as! AboutUsVC
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
            break
        case 1: // Our Beers
            squares[1].backgroundColor = UIColor(hexString: "#CA4D3C")
            buttons[1].setTitleColor(UIColor(hexString: "#CA4D3C"), for: .normal)
            selectionViews[1].isHidden = false
            
            
            let vc = storyboard!.instantiateViewController(withIdentifier: "OurBeers") as! OurBeersVC
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
            break
        case 2: // Podcast
            squares[2].backgroundColor = UIColor(hexString: "#CA4D3C")
            buttons[2].setTitleColor(UIColor(hexString: "#CA4D3C"), for: .normal)
            selectionViews[2].isHidden = false
            
            let vc = storyboard!.instantiateViewController(withIdentifier: "Podcast") as! PodcastsVC
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
            break
        case 3: // Contact Us
            squares[3].backgroundColor = UIColor(hexString: "#CA4D3C")
            buttons[3].setTitleColor(UIColor(hexString: "#CA4D3C"), for: .normal)
            selectionViews[3].isHidden = false
            
            
            let vc = storyboard!.instantiateViewController(withIdentifier: "Contact") as! ContactUsVC
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
            break
        case 4: // About the Developer
            let vc = storyboard!.instantiateViewController(withIdentifier: "Developer") as! DeveloperVC
            let navigationController = UINavigationController(rootViewController: vc)
            self.present(navigationController, animated: true, completion: nil)
            break
        default:
            print("Dashboard: Error with buttonClicked() tag")
        }
    }
    
}

// MapView Setup
extension DashVC: MKMapViewDelegate {
  
  func mapView(
    _ mapView: MKMapView,
    viewFor annotation: MKAnnotation
  ) -> MKAnnotationView? {
    
    guard let annotation = annotation as? MapLocation else {
      return nil
    }
    let identifier = "maplocation"
    var view: MKMarkerAnnotationView
    
    if let dequeuedView = mapView.dequeueReusableAnnotationView(
      withIdentifier: identifier) as? MKMarkerAnnotationView {
      dequeuedView.annotation = annotation
      view = dequeuedView
    } else {
      view = MKMarkerAnnotationView(
        annotation: annotation,
        reuseIdentifier: identifier)
      view.canShowCallout = true
      view.calloutOffset = CGPoint(x: -5, y: 5)
      view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    return view
  }
    
    func mapView(
      _ mapView: MKMapView,
      annotationView view: MKAnnotationView,
      calloutAccessoryControlTapped control: UIControl
    ) {
      guard let mapLocation = view.annotation as? MapLocation else {
        return
      }

      let launchOptions = [
        MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
      ]
      mapLocation.mapItem?.openInMaps(launchOptions: launchOptions)
    }
}


