//
//  BeerView.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/11/20.
//

import UIKit

class BeerView: UIView {

    @IBOutlet weak var beerIV: UIImageView!
    @IBOutlet weak var beerTitleLbl: UILabel!
    @IBOutlet weak var beerDescriptionLbl: UITextView!
    @IBOutlet weak var abvLbl: UILabel!
    @IBOutlet weak var ingredientsLbl: UILabel!
    @IBOutlet weak var locateBeerBtn: UIImageView!
    
    
    
}
