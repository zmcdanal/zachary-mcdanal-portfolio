package com.r0cker.interstellar.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.r0cker.interstellar.R;
import com.r0cker.interstellar.listeners.OpenFragment;

public class Fragment_Podcast extends Fragment implements View.OnClickListener {

    // Various Variables
    private static final String TAG = "Fragment_Podcast";
    OpenFragment mOpenFragmentListener;

    public Fragment_Podcast() {
    }

    public static Fragment_Podcast newInstance() {

        Bundle args = new Bundle();

        Fragment_Podcast fragment = new Fragment_Podcast();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //attaching button listener
        if (context instanceof OpenFragment) {
            mOpenFragmentListener = (OpenFragment) context;
        } else {
            Log.e(TAG, "onAttach: " + context.toString() + " must implement OpenFragment.Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_podcast, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {
            ImageButton podcastImgBtn = getView().findViewById(R.id.podcastImgBtn);
            ImageButton dismissBtn = getView().findViewById(R.id.dismissBtn);

            podcastImgBtn.setOnClickListener(this);
            dismissBtn.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.podcastImgBtn:
                // To Apple Podcast
                mOpenFragmentListener.openFrag(5, 0);
                break;
            case R.id.dismissBtn:
                // Dismiss Fragment back to dashboard
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStackImmediate();
                }
                break;
            default:
                Log.d(TAG, "onClick: Error - Default was triggered");
                break;
        }
    }
}
