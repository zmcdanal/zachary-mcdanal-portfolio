package com.r0cker.interstellar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.r0cker.interstellar.fragments.Fragment_AboutUs;
import com.r0cker.interstellar.fragments.Fragment_BeerMap;
import com.r0cker.interstellar.fragments.Fragment_ContactUs;
import com.r0cker.interstellar.fragments.Fragment_Dash;
import com.r0cker.interstellar.fragments.Fragment_Developer;
import com.r0cker.interstellar.fragments.Fragment_OurBeers;
import com.r0cker.interstellar.fragments.Fragment_Podcast;
import com.r0cker.interstellar.listeners.OpenFragment;
import com.r0cker.interstellar.objects.Beer;
import com.r0cker.interstellar.objects.BeerLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OpenFragment, PurchasesUpdatedListener{

    private static final String TAG = "MapsActivity";
    public static final String FILE_NAME = "beerdata.json";
    final ArrayList<Beer> allBeers = new ArrayList<>();
    private static final ArrayList<String> products = new ArrayList<>();
    BillingClient billingClient;
    final List<SkuDetails> skuDetailsList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        overridePendingTransition(R.anim.act_fadein, R.anim.act_fade_ou);

        if (products.isEmpty()) {
            Log.d(TAG, "onCreate: Here");
            products.add("com.r0cker.interstellar.tip1");
            products.add("com.r0cker.interstellar.tip2");
            products.add("com.r0cker.interstellar.tip3");
        }

        billingClient = BillingClient.newBuilder(this).setListener(this)
                .enablePendingPurchases()
                .build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                   // Toast.makeText(MapsActivity.this, "Succesfully connected to the billing client", Toast.LENGTH_SHORT).show();
                    SkuDetailsParams params = SkuDetailsParams.newBuilder()
                            .setSkusList(products).setType(BillingClient.SkuType.INAPP)
                            .build();


                    billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                        @Override
                        public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> list) {
                            Log.d(TAG, "Response code: " + billingResult.getResponseCode());
                            if (list != null && billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                                if (!skuDetailsList.isEmpty()) {
                                    skuDetailsList.clear();
                                }
                                for (SkuDetails skuDetails: list) {
                                    String price = skuDetails.getPrice();
                                    skuDetailsList.add(skuDetails);
                                    Log.d(TAG, "onSkuDetailsResponse: " + price);
                                }
                            }
                        }
                    });
                } else {
                    Log.d(TAG, "Failed to connect to the billing client");
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                Log.d(TAG, "Disconnected from the client");
            }
        });

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frag_container, Fragment_Dash.newInstance()).addToBackStack(TAG).commit();

        try {
            if (allBeers.isEmpty()) {
                pullDataFromFile();
            }
        } catch (Exception e) {
            Log.d(TAG, "onCreate: " + e);
        }
    }


    // All button logic for each fragment is handled here
    @Override
    public void openFrag(int which, int index) {
        switch (which) {
            case 0: // To About Us
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.act_fadein, R.anim.act_fade_ou, R.anim.act_fadein, R.anim.act_fade_ou)
                        .replace(R.id.frag_container, Fragment_AboutUs.newInstance()).addToBackStack(TAG).commit();
                break;
            case 1: // To Our Beers
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.act_fadein, R.anim.act_fade_ou, R.anim.act_fadein, R.anim.act_fade_ou)
                        .replace(R.id.frag_container, Fragment_OurBeers.newInstance(allBeers)).addToBackStack(TAG).commit();
                break;
            case 2: // To Podcast
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.act_fadein, R.anim.act_fade_ou, R.anim.act_fadein, R.anim.act_fade_ou)
                        .replace(R.id.frag_container, Fragment_Podcast.newInstance()).addToBackStack(TAG).commit();
                break;
            case 3: // To Contact Us
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.act_fadein, R.anim.act_fade_ou, R.anim.act_fadein, R.anim.act_fade_ou)
                        .replace(R.id.frag_container, Fragment_ContactUs.newInstance()).addToBackStack(TAG).commit();
                break;
            case 4: // To About the Developer
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.act_fadein, R.anim.act_fade_ou, R.anim.act_fadein, R.anim.act_fade_ou)
                        .replace(R.id.frag_container, Fragment_Developer.newInstance()).addToBackStack(TAG).commit();
                break;
            case 5:// To Podcast Webpage
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://thegingerbeerpodcast.podbean.com"));
                startActivity(browserIntent);
                break;
            case 6: // To Phone
                Uri number = Uri.parse("tel:12053351363");
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
                break;
            case 7: // To Email
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "InterstellarGinger@gmail.com" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Regarding Your Awesome Beer...");
                intent.putExtra(Intent.EXTRA_TEXT, "Dear Ginger Meister,\n\n");
                startActivity(Intent.createChooser(intent, ""));
                break;
            case 8: // To Facebook Page
                Intent facebookIntent = getOpenFacebookIntent(this);
                startActivity(facebookIntent);
                break;
            case 9: // To Instagram Page
                Intent instagramIntent = getOpenInstagramIntent(this);
                startActivity(instagramIntent);
                break;
            case 10: // To Beer Map
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.act_fadein, R.anim.act_fade_ou, R.anim.act_fadein, R.anim.act_fade_ou)
                        .replace(R.id.frag_container, Fragment_BeerMap.newInstance(allBeers.get(index))).addToBackStack(TAG).commit();
                break;
            case 11: // Tip button pressed
                BillingFlowParams params = BillingFlowParams.newBuilder()
                    .setSkuDetails(skuDetailsList.get(index))
                    .build();
                billingClient.launchBillingFlow(this, params);
                break;
            default:
                Log.d(TAG, "MapsActivity - openFrag: Error - Default was triggered");
                break;
        }
    }

    public static Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://facewebmodal/f?href=https://www.facebook.com/978067088947339")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/interstellarginger")); //catches and opens a url to the desired page
        }
    }

    public static Intent getOpenInstagramIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.instagram.android", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/_u/interstellargingerbeer")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/interstellargingerbeer")); //catches and opens a url to the desired page
        }
    }

    private void pullDataFromFile() throws IOException {
        String json;
        InputStream is = getApplicationContext().getAssets().open(FILE_NAME);

        int size = is.available();

        byte[] buffer = new byte[size];

        is.read(buffer);

        is.close();

        json = new String(buffer, StandardCharsets.UTF_8);

        try {
            JSONArray outerMostObject = new JSONArray(json);
            for (int i = 0; i < outerMostObject.length(); i++) {
                JSONObject b = outerMostObject.getJSONObject(i);
                JSONArray beers = b.getJSONArray("beers");
                for (int a = 0; a < beers.length(); a++) {
                    JSONObject beer = beers.getJSONObject(a);
                    String name = beer.getString("name");
                    String descrip = beer.getString("descrip");
                    String abv = beer.getString("abv");
                    ArrayList<String> ingredients = new ArrayList<>();
                    ArrayList<BeerLocation> locations = new ArrayList<>();

                    JSONArray ingred = beer.getJSONArray("ingredients");
                    for (int c = 0; c < ingred.length(); c++) {
                        String ingredient = ingred.getString(c);
                        ingredients.add(ingredient);
                    }

                    JSONArray loc = beer.getJSONArray("locations");
                    for (int d = 0; d < loc.length(); d++) {
                        JSONObject location = loc.getJSONObject(d);
                        String place = location.getString("place");
                        String address = location.getString("address");
                        String lat = location.getString("lat");
                        String lon = location.getString("lon");
                        Date currentTime = Calendar.getInstance().getTime();
                        Date date = Calendar.getInstance().getTime();
                        if (location.has("lastupdated")) {
                            String lastUpdated = location.getString("lastupdated");
                            String inputPattern = "MMM dd, yyyy";
                            SimpleDateFormat dateFormat = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
                            date = dateFormat.parse(lastUpdated);
                        }
                        Log.d(TAG, "pullDataFromFile: " + currentTime);
                        Calendar c = Calendar.getInstance();
                        c.setTime(currentTime);
                        // Check date on location. If 5 months back, don't add to map
                        c.add(Calendar.MONTH, -5);

                        if (c.getTime().before(date) || c.getTime().equals(date)) {
                            BeerLocation newLocation = new BeerLocation(place, address, lat, lon);
                            locations.add(newLocation);
                            Log.d(TAG, name + " at " + place + " was successfully added");
                        } else {
                            Log.d(TAG, name + " is no longer available at " + place + ": " + address + ", as of " + date + "");
                        }
                    }
                    Beer newBeer = new Beer(name, descrip, abv, ingredients, locations);
                    allBeers.add(newBeer);
                }
            }
        } catch (JSONException | ParseException e) {
            Log.d(TAG, "pullDataFromFile: " + e);
    }

    }

    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
        if (list != null && billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {

        }
        else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            Toast.makeText(this, "You have canceled the purchase", Toast.LENGTH_SHORT).show();
        }
    }
}