package com.r0cker.interstellar.objects;

import java.io.Serializable;

public class BeerLocation implements Serializable {

    // Variables
    private String place;
    private String address;
    private String lat;
    private String lon;

    // Constructor
    public BeerLocation(String place, String address, String lat, String lon) {
        this.place = place;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
    }

    // Getters
    public String getPlace() {
        return place;
    }

    public String getAddress() {
        return address;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    // Setters
    public void setPlace(String place) {
        this.place = place;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
