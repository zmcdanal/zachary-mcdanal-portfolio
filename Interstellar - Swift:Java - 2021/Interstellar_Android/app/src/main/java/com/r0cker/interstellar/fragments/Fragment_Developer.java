package com.r0cker.interstellar.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.r0cker.interstellar.R;
import com.r0cker.interstellar.listeners.OpenFragment;

public class Fragment_Developer extends Fragment implements View.OnClickListener {

    private static final String TAG = "Fragment_Developer";
    OpenFragment mOpenFragmentListener;

    public Fragment_Developer() {
    }

    public static Fragment_Developer newInstance() {

        Bundle args = new Bundle();

        Fragment_Developer fragment = new Fragment_Developer();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //attaching signout listener
        if (context instanceof OpenFragment) {
            mOpenFragmentListener = (OpenFragment) context;
        } else {
            Log.e(TAG, "onAttach: " + context.toString() + " must implement OpenFragment.Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_developer, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {

            // Dismiss Fragment back to dashboard
            ImageButton dismissBtn = getView().findViewById(R.id.dismissBtn);
            dismissBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStackImmediate();
                    }
                }
            });

            // Open Developer business card
            Button contactMeBtn = getView().findViewById(R.id.contactMeBtn);
            contactMeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://linqapp.com/zmcdanal"));
                    startActivity(browserIntent);
                }
            });

            // Tip button setup
            Button tip1 = getView().findViewById(R.id.purchase1);
            Button tip2 = getView().findViewById(R.id.purchase2);
            Button tip3 = getView().findViewById(R.id.purchase3);
            tip1.setOnClickListener(this);
            tip2.setOnClickListener(this);
            tip3.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.purchase1: // $5 tip
                mOpenFragmentListener.openFrag(11,0);
                break;
            case R.id.purchase2: // $10 tip
                mOpenFragmentListener.openFrag(11,1);
                break;
            case R.id.purchase3: // $50 tip
                mOpenFragmentListener.openFrag(11,2);
                break;
            default:
                Log.d(TAG, "Fragment_ Developer - onClick: Default was triggered");
                break;
        }
    }
}
