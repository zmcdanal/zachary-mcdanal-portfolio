package com.r0cker.interstellar.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.r0cker.interstellar.R;
import com.r0cker.interstellar.objects.Beer;
import com.r0cker.interstellar.objects.BeerLocation;

public class Fragment_BeerMap extends Fragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    // Various Variables
    private static final String TAG = "Fragment_BeerMap";
    private boolean infoWindowShowing = false;
    private Beer beer;

    public Fragment_BeerMap() {
    }

    public static Fragment_BeerMap newInstance(Beer beer) {

        Bundle args = new Bundle();
        args.putSerializable(TAG, beer);
        Fragment_BeerMap fragment = new Fragment_BeerMap();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_beermap, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Get map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.beerMap);
        mapFragment.getMapAsync(this);

        if (getView() != null && getArguments() != null) {

            // Get beer passed to fragment
            beer = (Beer)getArguments().getSerializable(TAG);

            // Dismiss Fragment back to dashboard
            ImageButton dismissBtn = getView().findViewById(R.id.dismissBtn);
            dismissBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStackImmediate();
                    }
                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Add a marker of brewery and move the camera
        LatLng alabaster = new LatLng(33.187689, -86.78175);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(alabaster, 7f));

        // Add all beer locations
        for (int i = 0; i < beer.getLocations().size(); i++) {
            BeerLocation location = beer.getLocations().get(i);
            LatLng coord = new LatLng(Double.parseDouble(location.getLat()), Double.parseDouble(location.getLon()));
            googleMap.addMarker(new MarkerOptions().position(coord).title(location.getPlace())
                    .snippet(location.getAddress()));
        }

        // Map Setup
        googleMap.setInfoWindowAdapter(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View contents = LayoutInflater.from(getContext())
                .inflate(R.layout.marker_info_window, null);

        ((TextView)contents.findViewById(R.id.title)).setText(marker.getTitle());
        ((TextView)contents.findViewById(R.id.address)).setText(marker.getSnippet());

        return contents;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        String label = "Interstellar";
        String uriBegin = "geo:" + marker.getPosition().latitude + "," + marker.getPosition().longitude;
        String query = marker.getPosition().latitude + "," + marker.getPosition().longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,uri);
        startActivity(intent);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (infoWindowShowing) {
            infoWindowShowing = false;
            marker.hideInfoWindow();
        } else {
            infoWindowShowing = true;
            marker.showInfoWindow();
        }
        return true;
    }


}
