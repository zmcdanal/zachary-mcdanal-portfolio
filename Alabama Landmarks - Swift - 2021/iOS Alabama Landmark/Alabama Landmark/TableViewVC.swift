//
//  ListViewVC.swift
//  Alabama Landmark
//
//  Created by Zakarias McDanal on 6/2/21.
//

import UIKit
import CoreLocation


class TableViewVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    // Variables
    var landmarks = [Landmark]()
    var sortedLandmarks = [Landmark]()
    var selectedLandmark: Landmark?
    let locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D(latitude: 33.1876830, longitude: -86.7818640)
    
    // Outlets
    @IBOutlet weak var photoTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        
        photoTableView.delegate = self
        photoTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])  {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        currentLocation = locValue
        // Sort Landmarks based on distance from user
        var fixed: [String: Double] = [:]
        for (idx, obj) in landmarks.enumerated() {
            let landmarkCoord = landmarks[idx].locations.coordinate
            let distance = landmarkCoord.distance(from: currentLocation)
            fixed[obj.name] = distance * 0.000621371192
        }
        let fixedSorted = fixed.sorted(by: { $0.1 < $1.1 })
        for sorted in fixedSorted {
            for mark in landmarks {
                if mark.name == sorted.key {
                    sortedLandmarks.append(mark)
                }
            }
        }
        photoTableView.reloadData()
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return landmarks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId") as! LandmarkCell
        
        if sortedLandmarks.isEmpty {
            cell.landmarkIV.image = UIImage(named: landmarks[indexPath.row].image)
            cell.landmarkTitle.text = landmarks[indexPath.row].name
            
            let landmarkCoord = landmarks[indexPath.row].locations.coordinate
            let distance = landmarkCoord.distance(from: currentLocation)
            
            cell.landmarkDist.text = "\(Double(round(10*(distance * 0.000621371192))/10))mi"
        } else {
        
        cell.landmarkIV.image = UIImage(named: sortedLandmarks[indexPath.row].image)
        cell.landmarkTitle.text = sortedLandmarks[indexPath.row].name
        
        let landmarkCoord = sortedLandmarks[indexPath.row].locations.coordinate
        let distance = landmarkCoord.distance(from: currentLocation)
        
        cell.landmarkDist.text = "\(Double(round(10*(distance * 0.000621371192))/10))mi"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedLandmark = sortedLandmarks[indexPath.row]
        performSegue(withIdentifier: ViewController.Segues.toDetailsVC, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewController.Segues.toDetailsVC {
            let destVC = segue.destination as! LandmarkDetailsVC
            destVC.landmark = selectedLandmark
        }
    }

    
}

extension CLLocationCoordinate2D {
    //distance in meters, as explained in CLLoactionDistance definition
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let destination=CLLocation(latitude:from.latitude,longitude:from.longitude)
        return CLLocation(latitude: latitude, longitude: longitude).distance(from: destination)
    }
}
