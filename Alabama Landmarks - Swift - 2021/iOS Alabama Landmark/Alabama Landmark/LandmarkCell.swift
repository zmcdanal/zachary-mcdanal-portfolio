//
//  LandmarkCell.swift
//  Alabama Landmark
//
//  Created by Zakarias McDanal on 6/3/21.
//

import UIKit

class LandmarkCell: UITableViewCell {

    // Outlets
    @IBOutlet weak var landmarkIV: UIImageView!
    @IBOutlet weak var landmarkTitle: UILabel!
    @IBOutlet weak var landmarkDist: UILabel!
    
    
    override func awakeFromNib() {
    // Tints the imageView making text easier to read
        let tintView = UIView()
        tintView.backgroundColor = UIColor(white: 0, alpha: 0.2) 
        tintView.frame = landmarkIV.bounds
        landmarkIV.addSubview(tintView)
    }
    
}
