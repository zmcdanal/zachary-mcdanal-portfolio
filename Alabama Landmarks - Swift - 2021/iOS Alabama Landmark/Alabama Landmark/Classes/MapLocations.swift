//
//  MapLocations.swift
//  Alabama Landmark
//
//  Created by Zakarias McDanal on 5/31/21.
//

import Foundation
import MapKit
import Contacts

class MapLocation: NSObject, MKAnnotation {
    let title: String?
    let locationAddress: String?
    let discipline: String?
    let coordinate: CLLocationCoordinate2D
    
    init(Title: String?, LocationAddress: String?, Discipline: String?, Coordinate: CLLocationCoordinate2D) {
        self.title = Title
        self.locationAddress = LocationAddress
        self.discipline = Discipline
        self.coordinate = Coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationAddress
    }
    
    var mapItem: MKMapItem? {
        guard let location = locationAddress else {
            return nil
        }
        
        let addressDict = [CNPostalAddressStreetKey: location]
        let placemark = MKPlacemark(
            coordinate: coordinate,
            addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
