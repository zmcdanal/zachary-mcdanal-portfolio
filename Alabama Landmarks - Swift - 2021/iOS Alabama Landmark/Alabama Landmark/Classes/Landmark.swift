//
//  Landmark.swift
//  Alabama Landmark
//
//  Created by Zakarias McDanal on 5/31/21.
//

import Foundation

class Landmark {
    var name: String
    var designated: String
    var address: String
    var description: String
    var website: String
    var image: String
    var locations: MapLocation
    
    init(Name: String, Designated: String, Address: String, Description: String, Website: String, Image: String, Locations: MapLocation) {
        self.name = Name
        self.designated = Designated
        self.address = Address
        self.description = Description
        self.website = Website
        self.image = Image
        self.locations = Locations
    }
}
