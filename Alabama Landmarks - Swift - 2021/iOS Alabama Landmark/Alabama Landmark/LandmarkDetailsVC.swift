//
//  LandmarkDetailsVC.swift
//  Alabama Landmark
//
//  Created by Zakarias McDanal on 6/3/21.
//

import UIKit
import MapKit

class LandmarkDetailsVC: UIViewController {
    
    // Variables
    var landmark: Landmark?
    
    // Outlets
    @IBOutlet weak var landmarkIV: UIImageView!
    @IBOutlet weak var titleTV: UILabel!
    @IBOutlet weak var descriptionTV: UILabel!
    @IBOutlet weak var designatedTV: UILabel!
    @IBOutlet weak var addressTV: UILabel!
    @IBOutlet weak var websiteTV: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateThePage()
        
    }
    

    func populateThePage() {
        if landmark != nil {
            landmarkIV.image = UIImage(named: landmark!.image)
            titleTV.text = landmark!.name
            descriptionTV.text = landmark!.description
            designatedTV.text = landmark!.designated
            addressTV.text = landmark!.locations.locationAddress
            websiteTV.text = landmark!.website
            
            if ((!landmark!.website.contains("N/A"))) {
                print("here")
                websiteTV.attributedText = NSAttributedString(string: landmark!.website, attributes:
                    [.underlineStyle: NSUnderlineStyle.single.rawValue])
                websiteTV.textColor = UIColor.blue
                let labelTapGesture = UITapGestureRecognizer(target: self, action: #selector(toWebsite(_:)))
                  websiteTV.isUserInteractionEnabled = true
                  websiteTV.addGestureRecognizer(labelTapGesture)
            } else {
                print("nope")
                websiteTV.isUserInteractionEnabled = false
            }

            designatedTV.sizeToFit()
        }
    }
    
    @objc func toWebsite(_ sender: UIGestureRecognizer) {
        print("boo")
        let url = URL(string: landmark!.website)!
        UIApplication.shared.open(url)
    }
    
    @IBAction func routeUser(_ sender: UIBarButtonItem) {
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: (landmark?.locations.coordinate)!, addressDictionary:nil))
        mapItem.name = landmark?.name
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    
}
