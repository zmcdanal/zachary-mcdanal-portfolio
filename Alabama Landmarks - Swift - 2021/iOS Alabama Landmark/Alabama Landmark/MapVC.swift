//
//  MapVC.swift
//  Alabama Landmark
//
//  Created by Zakarias McDanal on 6/3/21.
//

import UIKit
import MapKit

class MapVC: UIViewController {
    
    // Variables
    var landmarks = [Landmark]()
    var selectedLandmark: Landmark!
    
    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        mapView.isZoomEnabled = true
        // Do any additional setup after loading the view.
        
        // Set initial location in general brewery location
        let initialLocation = CLLocation(latitude: 33.1876830, longitude: -86.7818640)

        mapView.centerToLocation(initialLocation, regionRadius: 400000)
        
        for mark in landmarks {
            mapView.addAnnotation(mark.locations)
        }
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewController.Segues.toDetailsVC {
            let destVC = segue.destination as! LandmarkDetailsVC
            destVC.landmark = selectedLandmark
        }
    }

}


// Further MapView setup
extension MKMapView {
  func centerToLocation (_ location: CLLocation, regionRadius: CLLocationDistance) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}


extension MapVC: MKMapViewDelegate {
    
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    guard let annotation = annotation as? MapLocation else {
      return nil
    }
    
    let identifier = "Landmark"
    var view: MKMarkerAnnotationView

    if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
      dequeuedView.annotation = annotation
      view = dequeuedView
    } else {
      view = MKMarkerAnnotationView(
        annotation: annotation,
        reuseIdentifier: identifier)
      view.canShowCallout = true
      view.calloutOffset = CGPoint(x: -5, y: 5)
      view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    return view
  }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let mapLocation = view.annotation as? MapLocation else {
          return
        }
        for i in landmarks {
            if i.locations == mapLocation {
                selectedLandmark = i
            }
        }
        performSegue(withIdentifier: ViewController.Segues.toDetailsVC, sender: self)
      
    }
}
