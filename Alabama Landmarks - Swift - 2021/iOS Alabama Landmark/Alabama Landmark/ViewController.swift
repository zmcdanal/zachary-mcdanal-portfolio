//
//  ViewController.swift
//  Alabama Landmark
//
//  Created by Zakarias McDanal on 5/27/21.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    // Variables
    var landmarks = [Landmark]()
    var selectedLandmark: Landmark!
    
    
    // Outlets
    @IBOutlet weak var test1: UIView!
    @IBOutlet weak var test2: UIView!
    @IBOutlet weak var segmentedController: UISegmentedControl!
    
    // Segue Identifiers
    enum Segues {
        static let toTableViewChildVC = "ToTableView"
        static let toMapChildVC = "ToMapView"
        static let toDetailsVC = "ToDetailsVC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        segmentedController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor(red: 255, green: 255, blue: 255)], for: .normal)
        segmentedController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.white], for: .selected)
        
    }
    
    

    @IBAction func switchViews(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            test1.alpha = 0
            test2.alpha = 1
        } else {
            test1.alpha = 1
            test2.alpha = 0
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segues.toTableViewChildVC {
            let destVC = segue.destination as! TableViewVC
            destVC.landmarks = buildLandmarkData()
        }
        if segue.identifier == Segues.toMapChildVC {
            let destVC = segue.destination as! MapVC
            destVC.landmarks = buildLandmarkData()
        }
        if segue.identifier == Segues.toDetailsVC {
            let destVC = segue.destination as! LandmarkDetailsVC
            destVC.landmark = selectedLandmark
        }
    }

    
    // Create all landmarks and store them in array.
    // Only 36 landmarks so no need to pull from api or json file.
    func buildLandmarkData() -> [Landmark] {
        
        if landmarks.isEmpty {
        // Landmark 1 - USS Alabama
        landmarks.append(Landmark(Name: "Battleship USS Alabama (BB-60)", Designated: "January 14, 1986", Address: "Mobile", Description: "One of two surviving South Dakota-class battleships, Alabama was commissioned in 1942 and spent forty months in active service in World War II's Pacific theater, earning nine battle stars over twenty-six engagements with the Japanese.", Website: "https://www.ussalabama.com", Image: "uss_alabama", Locations: MapLocation(Title: "Battleship USS Alabama (BB-60)", LocationAddress: "2703 Battleship Pkwy, Mobile, AL 36603", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 30.681918, longitude: -88.014371))))
        // Landmark 2 - Apalachicola Fort
            landmarks.append(Landmark(Name: "Apalachicola Fort", Designated: "July 19, 1964", Address: "Russell", Description: "Spain established this wattle and daub blockhouse on the Chattahoochee River in 1690, attempting to maintain influence among the Lower Creek Indians. It was used for one year, and destroyed by the Spanish when they abandoned it.", Website: "N/A", Image: "apalachicola_fort", Locations: MapLocation(Title: "Apalachicola Fort", LocationAddress: "Holy Trinity, AL", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.220563, longitude: -85.005599))))
        // Landmark 3 - Barton Hall
        landmarks.append(Landmark(Name: "Barton Hall", Designated: "November 7, 1973", Address: "Colbert", Description: "This structure, built in 1840, is described by the National Park Service as a 'unusually sophisticated' Greek Revival style plantation house. The interior contains a stairway that ascends in a series of double flights and bridge-like landings to an observatory on the rooftop that offers views of the plantation.", Website: "N/A", Image: "barton_hall", Locations: MapLocation(Title: "Barton Hall", LocationAddress: "West of Cherokee, AL", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.752494, longitude: -88.003339))))
        // Landmark 4 - Bethel Baptist Church, Parsonage, and Guard House
        landmarks.append(Landmark(Name: "Bethel Baptist Church", Designated: "April 5, 2005", Address: "Jefferson", Description: "This church served as the headquarters for the Alabama Christian Movement for Human Rights, an organization active in the Civil Rights Movement, from 1956 to 1961. It focused on legal and nonviolent direct action against segregated accommodations, transportation, schools and employment discrimination.", Website: "https://bethelcollegeville.org", Image: "bethel", Locations: MapLocation(Title: "Bethel Baptist Church", LocationAddress: "3200 28th Ave N, Birmingham, AL 35207", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 33.551806, longitude: -86.802028))))
        // Landmark 5 - Bottle Creek Site
        landmarks.append(Landmark(Name: "Bottle Creek Indian Mounds", Designated: "April 19, 1994", Address: "Baldwin", Description: "This archaeological site contains eighteen mounds from the Mississippian cultural period. Located on Mound Island within the Mobile-Tensaw river delta, the site was occupied between AD 1250 and 1550. Scholars believe that it functioned as a social, political, religious, and trade center for the Mobile Delta region and the central Gulf Coast.", Website: "N/A", Image: "bottle_creek", Locations: MapLocation(Title: "Bottle Creek Indian Mounds", LocationAddress: "Stockton, AL", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 30.995556, longitude: -87.937639))))
        // Landmark 6 - Brown Chapel A.M.E. Church
        landmarks.append(Landmark(Name: "Brown Chapel A.M.E. Church", Designated: "December 12, 1997", Address: "Dallas", Description: "This church was a starting point for the Selma to Montgomery marches in 1965, and it played a major role in the events that led to the adoption of the Voting Rights Act of 1965. The national reaction to Selma's 'Bloody Sunday March' is widely credited with making the passage of the Voting Rights Act politically viable in the United States Congress.", Website: "N/A", Image: "brown_chapel", Locations: MapLocation(Title: "Brown Chapel A.M.E. Church", LocationAddress: "410 Martin Luther King St, Selma, AL 36703", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.411869, longitude: -87.016053))))
        // Landmark 7 - City Hall
        landmarks.append(Landmark(Name: "Old City Hall", Designated: "November 7, 1973", Address: "Mobile", Description: "The Italianate style Old City Hall and Southern Market in Mobile was completed in 1857. This building exemplifies the 19th-century American trend toward structures that served multiple civic functions.", Website: "N/A", Image: "city_hall", Locations: MapLocation(Title: "Old City Hall", LocationAddress: "111 S Royal St, Mobile, AL 36602", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 30.689979, longitude: -88.040106))))
        // Landmark 8 - Henry D. Clayton House
        landmarks.append(Landmark(Name: "Henry D. Clayton House", Designated: "December 8, 1976", Address: "Barbour", Description: "This was the home of antitrust legislator Henry De Lamar Clayton, Jr. He was the author of the Clayton Antitrust Act, an act that prohibited particular types of conduct that were deemed to not be in the best interest of a competitive market. He was appointed as a Federal District Judge in 1914, and became recognized as an advocate for judicial reform.", Website: "N/A", Image: "henry_clayton_house", Locations: MapLocation(Title: "Henry D. Clayton House", LocationAddress: "Clayton, AL", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 31.865611, longitude: -85.452361))))
        // Landmark 9 - J.L.M. Curry Home
        landmarks.append(Landmark(Name: "J.L.M. Curry Home", Designated: "December 21, 1965", Address: "Talladega", Description: "This was the home of educator Jabez Lamar Monroe Curry. He played a large role in the expansion and improvement of the public school system and the establishment of training schools for teachers throughout the South.", Website: "N/A", Image: "jim_curry", Locations: MapLocation(Title: "J.L.M. Curry Home", LocationAddress: "Talladega, AL", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 33.455833, longitude: -86.044444))))
        // Landmark 10 - Dexter Avenue Baptist Church
        landmarks.append(Landmark(Name: "Dexter Avenue Baptist Church", Designated: "May 30, 1974", Address: "Montgomery", Description: "Martin Luther King Jr. was the pastor of this church from 1954 to 1960. The Montgomery Improvement Association, which was headed by Dr. King, had its headquarters in the church and organized the Montgomery bus boycott from this site in 1955", Website: "N/A", Image: "dexter_ave", Locations: MapLocation(Title: "Dexter Avenue Baptist Church", LocationAddress: "454 Dexter Ave, Montgomery, AL 36104", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.377473, longitude: -86.303146))))
        // Landmark 11 - USS Drum
            landmarks.append(Landmark(Name: "USS Drum (SS-228)", Designated: "January 14, 1986", Address: "Mobile", Description: "Launched on May 12, 1941, this was the first of the Gato-class submarines completed before World War II. It represents what was the standard design for American fleet submarines at the beginning of that war. The USS Drum sank fifteen Japanese ships and earned twelve battle stars.", Website: "https://www.ussalabama.com/explore/uss-drum/", Image: "uss_drum", Locations: MapLocation(Title: "USS Drum (SS-228)", LocationAddress: "2703 Battleship Pkwy, Mobile, AL 36603", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 30.67883, longitude: -88.016631))))
        // Landmark 12 - Episcopal Church of the Nativity
        landmarks.append(Landmark(Name: "Episcopal Church of the Nativity", Designated: "June 21, 1990", Address: "Madison", Description: "This Gothic Revival church was built in 1859, and is considered by the National Park Service as one of the most pristine examples of Ecclesiastical Gothic architecture in the South. It is also one of the least-altered structures designed by architect Frank Wills.", Website: "https://nativity.dioala.org", Image: "episcopal_church", Locations: MapLocation(Title: "Episcopal Church of the Nativity", LocationAddress: "208 Eustis Ave SE, Huntsville, AL 35801", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.730189, longitude: -86.58405))))
        // Landmark 13 - Alabama State Capitol
            landmarks.append(Landmark(Name: "Alabama State Capitol", Designated: "December 19, 1960", Address: "Montgomery", Description: "Delegates from six seceding Southern states met here on February 4, 1861. On February 8, they adopted a 'Constitution for the Provisional Government of the Confederate States of America.' Jefferson Davis was inaugurated on the west portico on February 18. The Congress of the Confederate States met here until May 22, 1861, when the capital moved to Richmond, Virginia", Website: "https://ahc.alabama.gov/alabama-state-capitol.aspx", Image: "state_capitol", Locations: MapLocation(Title: "Alabama State Capitol", LocationAddress: "Montgomery, AL 36104", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.375743, longitude: -86.30094))))
        // Landmark 14 - Fort Mitchell Historic Site
            landmarks.append(Landmark(Name: "Fort Mitchell Historic Site", Designated: "June 21, 1990", Address: "Russell", Description: "Fort Mitchell represents three periods of interaction with Native Americans. The first period is the martial aspect of Manifest Destiny, when the Creek Indian Nation was defeated and forced to concede land.; the second represents the Indian Factory; the last concerns U.S. government attempts to honor treaty obligations.", Website: "http://visitfortmitchell.org", Image: "fort_mitchell", Locations: MapLocation(Title: "Fort Mitchell Historic Site", LocationAddress: "Fort Mitchell, AL 36856", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.351944, longitude: -85.021667))))
        // Landmark 15 - Fort Morgan
            landmarks.append(Landmark(Name: "Fort Morgan", Designated: "December 19, 1960", Address: "Baldwin", Description: "Fort Morgan was completed in 1834 and was used by Confederate forces during the Battle of Mobile Bay. This battle resulted in the Union Navy's Admiral David Farragut taking Mobile Bay and sealing off the Port of Mobile to Confederate shipping.", Website: "http://www.fort-morgan.org", Image: "fort_morgan", Locations: MapLocation(Title: "Fort Morgan", LocationAddress: "110 AL-180, Gulf Shores, AL 36542", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 30.228056, longitude: -88.023056))))
        // Landmark 16 - Fort Toulouse
            landmarks.append(Landmark(Name: "Fort Toulouse", Designated: "October 9, 1960", Address: "Elmore", Description: "Fort Toulouse served as the easternmost outpost of colonial French Louisiana. It was established in 1717 at the confluence of the Coosa and Tallapoosa rivers, and was abandoned in 1763, after the Treaty of Paris. Andrew Jackson reestablished a fort here in 1814 following his defeat of the Creek Nation at the Battle of Horseshoe Bend.", Website: "https://fttoulousejackson.org", Image: "fort_toulouse", Locations: MapLocation(Title: "Fort Toulouse", LocationAddress: "2521 W Fort Toulouse Rd, Wetumpka, AL 36093", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.506619, longitude: -86.251569))))
        // Landmark 17 - Foster Auditorium
            landmarks.append(Landmark(Name: "Foster Auditorium", Designated: "April 5, 2005", Address: "Tuscaloosa", Description: "The Alabama National Guard, Federal marshals, and U.S. Attorney General Nicholas Katzenbach escorted Vivian Malone past Alabama governor George C. Wallace during his infamous 'Stand In The Schoolhouse Door' in front of this building in 1963. This was the first step in desegregating the University of Alabama and is seen as an important event in the Civil Rights Movement.", Website: "https://rolltide.com/sports/2016/6/10/facilities-fosterauditorium-html.aspx", Image: "foster", Locations: MapLocation(Title: "Foster Auditorium", LocationAddress: "801 6th Ave, Tuscaloosa, AL 35401", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 33.207778, longitude: -87.543889))))
        // Landmark 18 - Gaineswood
            landmarks.append(Landmark(Name: "Gaineswood", Designated: "November 7, 1973", Address: "Marengo", Description: "This Greek Revival mansion was designated a NHL because it is considered one of the most unusual examples of that architectural style in the United States. It was built over the course of eighteen years by amateur architect and planter Nathan Bryan Whitfield. It is one of the few Greek Revival homes that features the Doric, Ionic, and Corinthian orders of architecture.", Website: "https://ahc.alabama.gov/properties/gaineswood/gaineswood.aspx", Image: "gaineswood", Locations: MapLocation(Title: "Gaineswood", LocationAddress: "805 S Cedar Ave, Demopolis, AL 36732", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.508726, longitude: -87.835239))))
        // Landmark 19 - Government Street Presbyterian Church
            landmarks.append(Landmark(Name: "Government Street Presbyterian Church", Designated: "October 5, 1992", Address: "Mobile", Description: "This church was built in 1836 and is one of the oldest and least-altered Greek Revival church buildings in the United States. The architectural design is by James Gallier, James Dakin, and Charles Dakin.", Website: "https://www.gspcmobile.org", Image: "govt_street", Locations: MapLocation(Title: "Government Street Presbyterian Church", LocationAddress: "300 Government St, Mobile, AL 36602", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 30.689153, longitude: -88.044151))))
        // Landmark 20 - Ivy Green
        landmarks.append(Landmark(Name: "Ivy Green", Designated: "March 31, 1992", Address: "Colbert", Description: "This site is where deaf and blind Helen Keller was born and learned to communicate, with the aid of her teacher and constant companion, Anne Sullivan.", Website: "https://www.helenkellerbirthplace.org", Image: "ivy_green", Locations: MapLocation(Title: "Ivy Green", LocationAddress: "300 N Commons St W, Tuscumbia, AL 35674", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.739444, longitude: -87.706667))))
        // Landmark 21 - Kenworthy Hall
            landmarks.append(Landmark(Name: "Kenworthy Hall", Designated: "August 18, 2004", Address: "Perry", Description: "This plantation house was completed in 1860 and is one of the best preserved examples of Richard Upjohn's distinctive asymmetrical Italian villa style. It is the only surviving residential example of Upjohn's Italian villa style that was especially designed to suit the Southern climate and the plantation lifestyle.", Website: "N/A", Image: "kenworthy", Locations: MapLocation(Title: "Kenworthy Hall", LocationAddress: "23200 AL-14, Marion, AL 36756", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.6351, longitude: -87.352))))
        // Landmark 22 - Old Monroe County Courthouse
            landmarks.append(Landmark(Name: "Old Monroe County Courthouse", Designated: "January 13, 2021", Address: "Monroe", Description: "1903 courthouse, known for its literary association with Truman Capote and Harper Lee.", Website: "https://www.monroecountymuseum.org/old-courthouse-museum", Image: "old_monroe", Locations: MapLocation(Title: "Old Monroe County Courthouse", LocationAddress: "31 N Alabama Ave, Monroeville, AL 36460", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 31.5271, longitude: -87.3243))))
        // Landmark 23 - Montgomery (snagboat)
            landmarks.append(Landmark(Name: "Montgomery (snagboat)", Designated: "June 30, 1989", Address: "Pickens", Description: "One of the few surviving steam-powered sternwheelers in the United States, it is one of two surviving United States Army Corps of Engineers snagboats. It was built in 1925 and played a major role in building the Alabama–Tombigbee–Tennessee River Project.", Website: "N/A", Image: "snagboat", Locations: MapLocation(Title: "Montgomery (snagboat)", LocationAddress: "Pickensville, AL 35447", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 33.2238, longitude: -88.2599))))
        // Landmark 24 - Montgomery Union Station
        landmarks.append(Landmark(Name: "Montgomery Union Station", Designated: "December 8, 1976", Address: "Montgomery", Description: "Constructed in 1898, this is an example of late 19th-century commercial architecture. It served as the focal point of transportation into Montgomery. The train shed is significant in that it shows the adaptation of bridge-building techniques to shelter structures, an important step in the history of American engineering.", Website: "N/A", Image: "union_station", Locations: MapLocation(Title: "Montgomery Union Station", LocationAddress: "210 Water St, Montgomery, AL 36104", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.3787, longitude: -86.3145))))
        // Landmark 25 - Moundville Archaeological Site
            landmarks.append(Landmark(Name: "Moundville Archaeological Site", Designated: "July 19, 1964", Address: "Hale", Description: "Moundville was first settled in the 10th century and represents a major period of Mississippian culture in the Southern United States. It acted as the center for a southerly diffusion of this culture toward the Gulf Coast. It was the second largest site of the classic Middle Mississippian era, after Cahokia in Illinois.", Website: "https://moundville.museums.ua.edu/ancient-site/", Image: "moundville", Locations: MapLocation(Title: "Moundville Archaeological Site", LocationAddress: "634 Mound State Parkway, Moundville, AL 35474", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.1, longitude: -85.1))))
        // Landmark 26 - Neutral Buoyancy Simulator
            landmarks.append(Landmark(Name: "Neutral Buoyancy Simulator", Designated: "October 3, 1985", Address: "Madison", Description: "This structure was built in 1955 to provide a simulated zero-gravity environment in which engineers, designers, and astronauts could perform the various phases of research needed to gain firsthand knowledge concerning design and operation problems associated with working in space. It contributed significantly to the United States space program, especially Project Gemini, the Apollo program, Skylab, and the Space Shuttle.", Website: "https://www.nasa.gov/centers/marshall/history/gallery/msfc_iow_7.html", Image: "simulator", Locations: MapLocation(Title: "Neutral Buoyancy Simulator", LocationAddress: "Huntsville, Alabama", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.652006, longitude: -86.678076))))
        // Landmark 27 - Edmund Pettus Bridge
            landmarks.append(Landmark(Name: "Edmund Pettus Bridge", Designated: "February 27, 2013", Address: "Dallas", Description: "This bridge across the Alabama River is noted for being the site of a bloody encounter during the 1965 Selma to Montgomery marches, an event influential in the passage of that year's Voting Rights Act.", Website: "N/A", Image: "edmund", Locations: MapLocation(Title: "Edmund Pettus Bridge", LocationAddress: "Selma, AL 36703", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.405556, longitude: -87.018611))))
        // Landmark 28 - Propulsion and Structural Test Facility
        landmarks.append(Landmark(Name: "Propulsion and Structural Test Facility", Designated: "October 3, 1985", Address: "Madison", Description: "This site was built in 1957 by the Army Ballistic Missile Agency and was the primary center responsible for the development of large vehicles and rocket propulsion systems. The Saturn Family of launch vehicles was developed here under the direction of Wernher von Braun. The Saturn V remains the most powerful launch vehicle ever brought to operational status, from a height, weight and payload standpoint.", Website: "N/A", Image: "propulsion", Locations: MapLocation(Title: "Propulsion and Structural Test Facility", LocationAddress: "Marshall Space Flight Center, Huntsville, Alabama", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.623636, longitude: -86.65855))))
        // Landmark 29 - Redstone Test Stand
        landmarks.append(Landmark(Name: "Redstone Test Stand", Designated: "October 3, 1985", Address: "Madison", Description: "This steel frame structure was built in 1953 and is the oldest static firing facility at the Marshall Space Flight Center. It was important in the development of the Jupiter-C and Mercury/Redstone vehicles that launched the first U.S. satellite and the first U.S. manned spaceflight.", Website: "https://www.nps.gov/articles/redstone-test-stand.htm", Image: "redstone", Locations: MapLocation(Title: "Redstone Test Stand", LocationAddress: "5298 Redstone Arsenal, Huntsville, AL 35808", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.630872, longitude: -86.666593))))
        // Landmark 30 - St. Andrew's Episcopal Church
        landmarks.append(Landmark(Name: "St. Andrew's Episcopal Church", Designated: "November 7, 1973", Address: "Hale", Description: "This small Carpenter Gothic church, with wooden buttresses, was built in 1853, and shows the influence of 19th-century architectural leader Richard Upjohn. It is considered one of the Southeast's outstanding examples of the picturesque movement in American church building", Website: "N/A", Image: "st_andrews", Locations: MapLocation(Title: "St. Andrew's Episcopal Church", LocationAddress: "Co Rd 12, Gallion, AL 36742", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.5091, longitude: -87.7014))))
        // Landmark 31 - Saturn V dynamic test stand
            landmarks.append(Landmark(Name: "Saturn V dynamic test stand", Designated: "October 3, 1985", Address: "Madison", Description: "Built in 1964 to conduct mechanical and vibrational tests on the fully assembled Saturn V rocket; major problems capable of causing failure of the vehicle were discovered and corrected here.", Website: "https://www.nps.gov/articles/saturn-v-dynamic-test-stand.htm", Image: "saturn", Locations: MapLocation(Title: "Saturn V dynamic test stand", LocationAddress: "Huntsville, Alabama", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.6291, longitude: -86.6611))))
        // Landmark 32 - Saturn V dynamic test vehicle
        landmarks.append(Landmark(Name: "Saturn V dynamic test vehicle", Designated: "February 10, 1987", Address: "Madison", Description: "This was the prototype for the Saturn V launch vehicle and was the first Saturn V constructed by the Marshall Space Flight Center under the direction of Dr. Wernher von Braun. It served as the test vehicle for the Saturn support facilities at the Marshall Space Flight Center.", Website: "N/A", Image: "saturn_vehicle", Locations: MapLocation(Title: "Saturn V dynamic test vehicle", LocationAddress: "Huntsville, Alabama", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.7082, longitude: -86.6558))))
        // Landmark 33 - 16th Street Baptist Church
            landmarks.append(Landmark(Name: "16th Street Baptist Church", Designated: "February 20, 2006", Address: "Jefferson", Description: "This church was used as a meeting place, training center, and as a departure point for marches during the Civil Rights Movement. It was the site of a bombing by the Ku Klux Klan on September 15, 1963, in which four young girls were killed and twenty-two others were injured.", Website: "https://www.16thstreetbaptist.org", Image: "16th_street", Locations: MapLocation(Title: "16th Street Baptist Church", LocationAddress: "1530 6th Ave N, Birmingham, AL 35203", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 33.51658, longitude: -86.81481))))
        // Landmark 34 - Sloss Furnaces
            landmarks.append(Landmark(Name: "Sloss Furnaces", Designated: "May 29, 1981", Address: "Jefferson", Description: "Built from 1881 to 1882, this is the oldest remaining blast furnace in the state. Its NHL designation represents Alabama's early 20th-century preeminence in the production of pig iron and cast iron, an example of a post-Civil War effort to industrialize the agrarian South.", Website: "https://www.slossfurnaces.com", Image: "sloss", Locations: MapLocation(Title: "Sloss Furnaces", LocationAddress: "20 32nd St N, Birmingham, AL 35222", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 33.520655, longitude: -86.791306))))
        // Landmark 35 - Swayne Hall, Talladega College
            landmarks.append(Landmark(Name: "Swayne Hall, Talladega College", Designated: "December 2, 1974", Address: "Talladega", Description: "Swayne Hall was built in 1857 as a Baptist men's college. Following the American Civil War, it became a part of Talladega College, Alabama's oldest private, historically black, liberal arts college.", Website: "http://www.talladega.edu/our-history/", Image: "swayne_hall", Locations: MapLocation(Title: "Swayne Hall, Talladega College", LocationAddress: "627 W Battle St, Talladega, AL 35160", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 33.4336, longitude: -86.1133))))
        // Landmark 36 - Tuskegee University
        landmarks.append(Landmark(Name: "Tuskegee University", Designated: "June 23, 1965", Address: "Macon", Description: "One of the best known African American universities in the United States, Tuskegee was founded by Booker T. Washington in 1881. It began with a curriculum designed to provide industrial and vocational education to African Americans and featured such acclaimed educators as George Washington Carver.[46] Tuskegee Institute is both a National Historic Landmark and a National Historic Site.", Website: "https://www.tuskegee.edu", Image: "tuskegee", Locations: MapLocation(Title: "Tuskegee University", LocationAddress: "1200 W Montgomery Rd, Tuskegee, AL 36088", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.4303, longitude: -85.7078))))
        // Landmark 37 - Frank M. Johnson Jr. Federal Building and United States Courthouse
            landmarks.append(Landmark(Name: "Frank M. Johnson Jr. Federal Building and United States Courthouse", Designated: "July 21, 2015", Address: "Montgomery", Description: "Many key civil rights cases, including the bus boycott litigation, heard in this 1933 building.", Website: "https://www.gsa.gov/historic-buildings/frank-m-johnson-jr-federal-building-and-us-courthouse-montgomery-al", Image: "frank_johnson", Locations: MapLocation(Title: "Frank M. Johnson Jr. Federal Building and United States Courthouse", LocationAddress: "15 Lee St, Montgomery, AL 36104", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.375, longitude: -86.3095))))
        // Landmark 38 - Wilson Dam
            landmarks.append(Landmark(Name: "Wilson Dam", Designated: "November 13, 1966", Address: "Colbert & Lauderdake", Description: "Wilson Dam, on the Tennessee River, was built between 1918 and 1925 by the United States Army Corps of Engineers and later came under the control of the Tennessee Valley Authority (TVA). It is the oldest of TVA's hydroelectric dams.", Website: "https://www.tva.com/energy/our-power-system/hydroelectric/wilson", Image: "wilson_dam", Locations: MapLocation(Title: "Wilson Dam", LocationAddress: "Florence, AL 35630", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 34.8008, longitude: -87.6258))))
        // Landmark 39 - Yuchi Town Site
        landmarks.append(Landmark(Name: "Yuchi Town Site", Designated: "June 19, 1996", Address: "Russell", Description: "This archaeological site was occupied by the Apalachicola and Yuchi tribes. During the 17th century, the Apalachicola tribe allied with the Spanish in Florida against the English in Carolina and were ultimately destroyed as a culture. The Yuchi tribe settled here later and constantly shifted their alliances with various European powers, until they were displaced by the expanding American frontier in the Southeast in the early 19th century.", Website: "N/A", Image: "yuchi", Locations: MapLocation(Title: "Yuchi Town Site", LocationAddress: "Fort Benning, Russell County, Alabama", Discipline: "Sculpture", Coordinate: CLLocationCoordinate2D(latitude: 32.3, longitude: -84.9833))))
        }
        return landmarks
    }
}

extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }
}

